Practice for tech interviews that require leetcode problems.

## How to use:

Requires a C++20 compiler, meson and gtest
```
git clone https://gitlab.com/agravgaard/leetcode-practice-problems.git
cd leetcode-practice-problems
meson setup build
meson compile -C build
meson test -C build
```

## [Interview prepeparation](interview/README.md)

* [System design](interview/sys_design.md)
  - [My answers](interview/sys_design_answers.md)
* [Behaviour questions](interview/behaviour_questions.md)
  - [My answers](interview/behaviour_questions_answers.md)
* [Keywords and questions](interview/keywords_and_questions.md)
  - [My answers](interview/keywords_and_questions_answers.md)
* [Behavioural question process](behavioural_question_process.md)
  - [My answers](interview/behavioural_question_process_answers.md)
