/**
 * Definition for a binary tree node.
 */
struct TreeNode {
  int val{0};
  TreeNode *left{nullptr};
  TreeNode *right{nullptr};
  TreeNode() = default;
  explicit TreeNode(int x) : val(x) {}
  TreeNode(int x, TreeNode *left, TreeNode *right)
      : val(x), left(left), right(right) {}
};

#include <cmath>
#include <numeric>
#include <string>
#include <vector>

#include <gtest/gtest.h>

using matrix_t = std::vector<std::vector<std::string>>;

auto find_depth(const TreeNode *node) -> size_t {
  if (node == nullptr) {
    return 0U;
  }
  return std::max(find_depth(node->left) + 1U, find_depth(node->right) + 1U);
}

auto fill_recurse(const TreeNode *node, matrix_t *mat, size_t i, size_t j) {
  if (node == nullptr) {
    return;
  }
  std::cerr << "i,j = " << i << "," << j << ", val = " << node->val << "\n";
  mat->at(i).at(j) = std::to_string(node->val);
  ++i;
  const auto m = mat->size();
  const auto level_exp = static_cast<int>(m - i) - 1;
  if (level_exp < 0) {
    return;
  }
  const auto half_j = std::exp2(level_exp);
  fill_recurse(node->left, mat, i, j - half_j);
  fill_recurse(node->right, mat, i, j + half_j);
}

constexpr auto two_power_sum(size_t m) -> size_t {
  auto out = 0U;
  for (auto i = 0U; i < m; ++i) {
    out += static_cast<size_t>(std::exp2(i));
  }
  return out;
}

class Solution {
public:
  static auto printTree(const TreeNode *root) -> matrix_t {
    const auto m = find_depth(root);
    const auto n = two_power_sum(m - 1) * 2 + 1;
    matrix_t out(m);
    for (auto &row : out) {
      row.resize(n);
    }
    fill_recurse(root, &out, 0, n / 2);

    return out;
  }
};

TEST(LeetCodeTests, 655) { // NOLINT
  constexpr auto five = 5;
  constexpr auto six = 6;
  constexpr auto seven = 7;
  auto node = std::make_unique<TreeNode>(five);
  auto node3 = std::make_unique<TreeNode>(3);
  auto node6 = std::make_unique<TreeNode>(six);
  auto node2 = std::make_unique<TreeNode>(2);
  auto node4 = std::make_unique<TreeNode>(4);
  auto node7 = std::make_unique<TreeNode>(seven);
  node->left = node3.get();
  node->right = node6.get();
  node->left->left = node2.get();
  node->left->right = node4.get();
  node->right->right = node7.get();

  Solution::printTree(node.get());
}
