#include <algorithm>
#include <map>
#include <memory>
#include <vector>

#include <gtest/gtest.h>

/**
 * Definition for a binary tree node.
 * from leetCode with changes to default values
 * */
constexpr auto align = 32;
struct TreeNode {
  int val = 0;
  TreeNode *left = nullptr;
  TreeNode *right = nullptr;
  explicit TreeNode(int x) : val(x) {}
  TreeNode(int x, TreeNode *left, TreeNode *right)
      : val(x), left(left), right(right) {}
} __attribute__((packed)) __attribute__((aligned(align)));

class Solution {
public:
  static auto inorderTraversal(TreeNode *root) -> std::vector<int> {
    std::vector<int> out;
    TreeNode *cur_node = root;

    while (cur_node != nullptr) {
      if (cur_node->left != nullptr) {
        // Find the inorder predecessor of current
        TreeNode *predecessor = cur_node->left;

        // ifa) Set right child of the rightmost node in current's
        // left subtree to be cur_node.
        // Unless already done, in that case, set it back to nullptr and push
        // cur_node->val
        while (predecessor->right != nullptr &&
               predecessor->right != cur_node) {
          // second condition used when reverting
          predecessor = predecessor->right;
        }
        if (predecessor->right == nullptr) {
          predecessor->right = cur_node;
          cur_node = cur_node->left; // ifb) Go to this left child
        } else {
          // Revert back the changes
          predecessor->right = nullptr;
          out.push_back(cur_node->val);
          cur_node = cur_node->right;
        }
      } else {
        out.push_back(cur_node->val); // ea)
        cur_node = cur_node->right;   // eb)
      }
    }
    return out;
  }
};

TEST(LeetCodeTests, 94) { // NOLINT
  auto node_3 = std::make_unique<TreeNode>(3);
  auto node_2 = std::make_unique<TreeNode>(2, node_3.get(), nullptr);
  auto test_input = std::make_unique<TreeNode>(1, nullptr, node_2.get());

  const auto true_output = std::vector<int>{1, 3, 2};

  EXPECT_EQ(Solution::inorderTraversal(test_input.get()), true_output);
}
