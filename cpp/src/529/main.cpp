#include <string>
#include <vector>

#include <gtest/gtest.h>

using matrix_t = std::vector<std::vector<char>>;
/*
 * 'M' represents an unrevealed mine
 * 'E' represents an unrevealed empty square
 * 'B' represents a revealed blank square that has no adjacent (above, below,
 * left, right, and all 4 diagonals) mines digit ('1' to '8') represents how
 * many mines are adjacent to this revealed square 'X' represents a revealed
 * mine.
 */

auto get_val(const matrix_t &mat, const std::vector<int> &pos) -> char {
  return mat.at(pos.at(0)).at(pos.at(1));
}
auto set_val(matrix_t *mat, const std::vector<int> &pos, char new_val) {
  mat->at(pos.at(0)).at(pos.at(1)) = new_val;
}

auto count_mines(const matrix_t &mat, const std::vector<int> &pos) -> char {
  auto out = 0;
  auto x = pos.at(0);
  auto y = pos.at(1);
  const auto nrows = mat.size();
  const auto ncols = mat.at(0).size();
  for (auto i = std::max(0, x - 1); i < std::min(static_cast<int>(nrows), x + 2); ++i) {
    for (auto j = std::max(0, y - 1); j < std::min(static_cast<int>(ncols), y + 2); ++j) {
      if (i == x && j == y) {
        continue;
      }
      switch (get_val(mat, {i, j})) {
      case 'M':
      case 'X':
        ++out;
      default:
        break;
      }
    }
  }
  return out;
}

auto flip_around(matrix_t *mat, const std::vector<int> &pos) {
  const auto n_mines = count_mines(*mat, pos);
  if (n_mines != 0) {
    mat->at(pos.at(0)).at(pos.at(1)) = std::to_string(n_mines)[0];
    return;
  }

  auto x = pos.at(0);
  auto y = pos.at(1);
  set_val(mat, pos, 'B');
  const auto nrows = mat->size();
  const auto ncols = mat->at(0).size();
  for (auto i = std::max(0, x - 1); i < std::min(static_cast<int>(nrows), x + 2); ++i) {
    for (auto j = std::max(0, y - 1); j < std::min(static_cast<int>(ncols), y + 2); ++j) {
      switch (get_val(*mat, {i, j})) {
      case 'E':
        flip_around(mat, {i, j});
        break;
      default:
        break;
      }
    }
  }
}

class Solution {
public:
  static auto updateBoard(const matrix_t &board, const std::vector<int> &click)
      -> matrix_t {
    matrix_t board_out = board;
    const auto c_val = get_val(board_out, click);
    switch (c_val) {
    case 'M':
      set_val(&board_out, click, 'X');
      break;
    case 'E':
      flip_around(&board_out, click);
      break;
    default:
      break;
    }
    return board_out;
  }
};

TEST(LeetCodeTests, 529) { // NOLINT
  std::vector<std::tuple<matrix_t, std::vector<int>, matrix_t>> test_case = {
      std::make_tuple<matrix_t, std::vector<int>, matrix_t>(
          {{'E', 'E', 'E', 'E', 'E'},
           {'E', 'E', 'M', 'E', 'E'},
           {'E', 'E', 'E', 'E', 'E'},
           {'E', 'E', 'E', 'E', 'E'}},
          {3, 0},
          {{'B', '1', 'E', '1', 'B'},
           {'B', '1', 'M', '1', 'B'},
           {'B', '1', '1', '1', 'B'},
           {'B', 'B', 'B', 'B', 'B'}})};

  for (const auto &tc : test_case) {
    EXPECT_EQ(Solution::updateBoard(std::get<0>(tc), std::get<1>(tc)),
              std::get<2>(tc));
  }
}
