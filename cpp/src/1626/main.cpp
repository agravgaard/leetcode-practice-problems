#include <algorithm>
#include <numeric>
#include <vector>

#include <gtest/gtest.h>

struct player_t {
  int age;
  int score;
};

class Solution {
public:
  static auto bestTeamScore(const std::vector<int> &scores,
                            const std::vector<int> &ages) -> int {
    const auto n = scores.size();
    if (n == 1) {
      return scores.at(0);
    }

    std::vector<player_t> players(n);
    std::transform(scores.cbegin(), scores.cend(), ages.cbegin(),
                   players.begin(), [](int score, int age) {
                     return player_t{age, score};
                   });
    std::sort(players.begin(), players.end(), [](auto p1, auto p2) {
      return p1.age > p2.age || (!(p2.age > p1.age) && p1.score > p2.score);
    });

    int score = 0;
    std::vector<int> player_scores(n);
    for (auto i = 0U; i < n; ++i) {
      int pscore = players[i].score;
      player_scores[i] = pscore;
      for (auto j = 0U; j < i; ++j) {
        if (players[j].score >= players[i].score) {
          player_scores[i] =
              std::max(player_scores[i], player_scores[j] + pscore);
        }
      }
      score = std::max(score, player_scores[i]);
    }
    return score;
  }
};

TEST(LeetCodeTests, 1626) { // NOLINT
  const auto scores = std::vector<int>{4, 5, 6, 5};
  const auto ages = std::vector<int>{2, 1, 2, 1};
  const auto true_output = 16;

  EXPECT_EQ(Solution::bestTeamScore(scores, ages), true_output);

  const auto scores2 =
      std::vector<int>{596, 277, 897, 622, 500, 299, 34,  536, 797,
                       32,  264, 948, 645, 537, 83,  589, 770};
  const auto ages2 = std::vector<int>{18, 52, 60, 79, 72, 28, 81, 33, 96,
                                      15, 18, 5,  17, 96, 57, 72, 72};
  const auto true_output2 = 3287;

  EXPECT_EQ(Solution::bestTeamScore(scores2, ages2), true_output2);

  const auto scores3 = std::vector<int>{588, 992, 523, 155, 94,  683, 832, 806,
                                        198, 508, 424, 821, 658, 419, 509, 157,
                                        227, 875, 41,  331, 459, 819, 512};
  const auto ages3 =
      std::vector<int>{26, 81, 70, 81, 6,  60, 43, 18, 74, 95, 98, 25,
                       74, 62, 17, 17, 88, 68, 98, 22, 18, 33, 20};
  const auto true_output3 = 5378;

  EXPECT_EQ(Solution::bestTeamScore(scores3, ages3), true_output3);
}
