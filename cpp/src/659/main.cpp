#include <algorithm>
#include <vector>

#include <gtest/gtest.h>

class Solution {
public:
  static auto isPossible(const std::vector<int> &nums) -> bool {
    if (nums.size() < 3) {
      return false;
    }
    std::vector<std::pair<int, size_t>> chain_values;

    for (const auto &num : nums) {
      if (chain_values.empty()) {
        // start count with current num
        chain_values.emplace_back(num, 1);
      } else {
        const auto it_chain = std::find_if(
            chain_values.rbegin(), chain_values.rend(), [num](auto &chain) {
              if (chain.first == num - 1) {
                chain.first++;
                chain.second++;
                return true;
              }
              return false;
            });
        if (it_chain == chain_values.rend()) {
          chain_values.emplace_back(num, 1);
        }
      }
    }

    // all chains should be longer than 3:
    return std::find_if(chain_values.cbegin(), chain_values.cend(),
                        [](const auto &chain) { return chain.second < 3; }) ==
           chain_values.cend();
  }
};

TEST(LeetCodeTests, 659) { // NOLINT
  const auto test_cases = std::vector<std::tuple<std::vector<int>, bool>>{
      {{1, 2, 3, 3, 4, 5}, true},

  };

  for (const auto &tc : test_cases) {
    EXPECT_EQ(Solution::isPossible(std::get<0>(tc)), std::get<1>(tc));
  }
}
