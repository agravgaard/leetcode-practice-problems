#include <algorithm>
#include <map>
#include <vector>

#include <gtest/gtest.h>

using grid_t = std::vector<std::vector<char>>;
using map_t = std::map<std::pair<size_t, size_t>, size_t>;

auto grow_island(map_t *coord2island, size_t island_number, const grid_t &grid,
                 size_t i, size_t j) -> size_t {
  if (grid[i][j] == '0') {
    return island_number;
  }
  auto key = std::make_pair(i, j);
  // if already checked, return
  const auto it_exists = coord2island->try_emplace(key, island_number);
  // (false if key already exists and no insertion happened)
  if (!it_exists.second) {
    return island_number;
  }

  // check neighbours
  if (i > 0) {
    grow_island(coord2island, island_number, grid, i - 1, j);
  }
  if (j > 0) {
    grow_island(coord2island, island_number, grid, i, j - 1);
  }
  if (i + 1 < grid.size()) {
    grow_island(coord2island, island_number, grid, i + 1, j);
  }
  if (j + 1 < grid.at(0).size()) {
    grow_island(coord2island, island_number, grid, i, j + 1);
  }

  return island_number + 1;
}

class Solution {
public:
  static auto numIslands(const grid_t &grid) -> int {
    // Map x, y -> island id
    std::map<std::pair<size_t, size_t>, size_t> coord2island;
    auto island_number = size_t{0};
    for (auto i = size_t{0}; i < grid.size(); ++i) {
      for (auto j = size_t{0}; j < grid.at(0).size(); ++j) {
        island_number = grow_island(&coord2island, island_number, grid, i, j);
      }
    }
    return static_cast<int>(island_number);
  }
};

TEST(LeetCodeTests, 200) { // NOLINT
  const auto test_cases = std::vector<std::tuple<grid_t, int>>{
      {{{'1', '1', '1', '1', '0'},
        {'1', '1', '0', '1', '0'},
        {'1', '1', '0', '0', '0'},
        {'0', '0', '0', '0', '0'}},
       1},
      {{{'1', '1', '0', '0', '0'},
        {'1', '1', '0', '0', '0'},
        {'0', '0', '1', '0', '0'},
        {'0', '0', '0', '1', '1'}},
       3},
  };

  for (const auto &tc : test_cases) {
    EXPECT_EQ(Solution::numIslands(std::get<0>(tc)), std::get<1>(tc));
  }
}
