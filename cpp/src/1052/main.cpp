#include <algorithm>
#include <stack>
#include <string>
#include <vector>

#include <gtest/gtest.h>

class Solution {
public:
  static auto maxSatisfied(const std::vector<int> &customers,
                           const std::vector<int> &grumpy, int minutes) -> int {
    auto i_end = customers.size();
    auto max_satisfied = 0U;
    size_t best_i = 0U;
    // sliding window of size minutes, find max
    for (auto i = 0U; i <= i_end - minutes; ++i) {
      auto satisfied = 0U;
      for (auto j = i; j < i + minutes; ++j) {
        satisfied += grumpy[j] == 1 ? customers[j] : 0;
      }
      if (max_satisfied < satisfied) {
        max_satisfied = satisfied;
        best_i = i;
      }
    }

    max_satisfied = 0U;
    for (auto i = 0U; i < i_end; ++i) {
      if (i >= best_i && i < best_i + minutes) {
        max_satisfied += customers[i];
      } else {
        max_satisfied += grumpy[i] == 1 ? 0 : customers[i];
      }
    }

    return max_satisfied;
  }
};

TEST(LeetCodeTests, 1052) { // NOLINT
  constexpr auto seven = 7;
  constexpr auto five = 5;

  std::vector<int> test_customers = {1, 0, 1, 2, 1, 1, seven, five};
  std::vector<int> test_grumpy = {0, 1, 0, 1, 0, 1, 0, 1};

  constexpr auto test_minutes = 3;

  constexpr auto true_satisfied = 16;

  EXPECT_EQ(Solution::maxSatisfied(test_customers, test_grumpy, test_minutes),
            true_satisfied);
}
