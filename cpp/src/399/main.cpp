#include <algorithm>
#include <string>
#include <vector>

#include <gtest/gtest.h>

using pairlist = std::vector<std::vector<std::string>>;

auto generate_combos(std::map<std::string, double> *out_map,
                     const std::set<std::string> &unique_str) -> void {
  for (const auto &str : unique_str) {
    const auto sid = str + "-";
    const auto eid = "-" + str;
    for (const auto &[key, val] : *out_map) {
      if (sid.size() + 1 > key.size()) {
        // impossible combination
        continue;
      }
      // We already generated inverse, so only search top
      const auto ksid = key.substr(0, sid.size());
      if (ksid == sid) {
        const auto keid = key.substr(sid.size(), key.size()); // after "-"
        for (const auto &[key2, val2] : *out_map) {
          if (keid.size() + 2 > key2.size()) {
            // impossible combination
            continue;
          }
          // We already generated inverse, so only search top
          const auto ksid2 = key2.substr(0, keid.size() + 1);
          const auto neweid = key2.substr(keid.size(), key2.size());
          if (ksid2 == keid + "-" && neweid != eid) {
            out_map->insert({str + neweid, val * val2});
          }
        }
      }
    }
  }
}

auto generate_map(const pairlist &eq, const std::vector<double> &vals)
    -> std::map<std::string, double> {
  std::map<std::string, double> out_map;
  std::set<std::string> unique_str;
  auto i = 0;
  for (const auto &tb : eq) {
    unique_str.insert({tb[0], tb[1]});
    out_map.insert({tb[0] + "-" + tb[1], vals[i]});
    out_map.insert({tb[1] + "-" + tb[0], 1.0 / vals[i]});
    ++i;
  }
  auto map_size = out_map.size();
  while (true) {
    // It's cheaper than it looks
    generate_combos(&out_map, unique_str);
    if (out_map.size() == map_size) {
      break;
    }
    map_size = out_map.size();
  }

  for (const auto &str : unique_str) {
    const auto sid = str + "-";
    out_map.insert({sid + str, 1.0});
  }

  return out_map;
}

class Solution {
public:
  static auto calcEquation(const pairlist &equations,
                           const std::vector<double> &values,
                           const pairlist &queries) -> std::vector<double> {
    std::vector<double> results;
    const auto eq_map = generate_map(equations, values);
    for (const auto &query : queries) {
      const auto qstr = query[0] + "-" + query[1];
      if (eq_map.find(qstr) != eq_map.end()) {
        results.push_back(eq_map.at(qstr));
      } else {
        results.push_back(-1.0);
      }
    }

    return results;
  }
};

TEST(LeetCodeTests, 399) { // NOLINT

  const auto test_cases = std::vector<
      std::tuple<pairlist, std::vector<double>, pairlist, std::vector<double>>>{
      {{{"a", "b"}, {"b", "c"}},
       {2.0, 3.0},
       {{"a", "c"}, {"b", "a"}, {"a", "e"}, {"a", "a"}, {"x", "x"}},
       {6.00000, 0.50000, -1.00000, 1.00000, -1.00000}},
      {{{"a", "b"}, {"b", "c"}, {"bc", "cd"}},
       {1.5, 2.5, 5.0},
       {{"a", "c"}, {"c", "b"}, {"bc", "cd"}, {"cd", "bc"}},
       {3.75000, 0.40000, 5.00000, 0.20000}},
      {{{"a", "b"}},
       {0.5},
       {{"a", "b"}, {"b", "a"}, {"a", "c"}, {"x", "y"}},
       {0.50000, 2.00000, -1.00000, -1.00000}}};

  for (const auto &tc : test_cases) {
    EXPECT_EQ(Solution::calcEquation(std::get<0>(tc), std::get<1>(tc),
                                     std::get<2>(tc)),
              std::get<3>(tc));
  }
}
