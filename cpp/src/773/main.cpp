#include <array>
#include <queue>
#include <string>
#include <vector>

#include <gtest/gtest.h>

using board_t = std::vector<std::vector<int>>;

auto to_string(const board_t &board) -> std::string {
  return std::to_string(board.at(0).at(0)) + std::to_string(board.at(0).at(1)) +
         std::to_string(board.at(0).at(2)) + std::to_string(board.at(1).at(0)) +
         std::to_string(board.at(1).at(1)) + std::to_string(board.at(1).at(2));
}

class Solution {
public:
  static auto slidingPuzzle(const board_t &board) -> int {
    auto s_board = to_string(board);

    const std::array<std::vector<size_t>, 6> possible_moves{
        {{1, 3}, {0, 2, 4}, {1, 5}, {0, 4}, {3, 5, 1}, {4, 2}}};

    std::map<std::string, int> mem;
    mem[s_board] = 0;

    std::queue<std::pair<std::string, size_t>> q;
    q.push({s_board, s_board.find('0')});

    while (!q.empty() && q.front().first != "123450") {
      auto zero_pos = q.front().second;
      for (const auto &move_to : possible_moves.at(zero_pos)) {
        auto str = q.front().first; // current string
        // move
        std::swap(str[move_to], str[zero_pos]);

        // only continue if not seen before
        if (mem.find(str) == mem.end()) {
          mem[str] = mem[q.front().first] + 1;
          q.push({str, move_to});
        }
      }
      q.pop();
    }
    return q.empty() ? -1 : mem[q.front().first];
  }
};

TEST(LeetCodeTests, 773) { // NOLINT
  constexpr auto five = 5;
  const std::vector<std::tuple<std::vector<std::vector<int>>, int>> test_case =
      {
          {{{1, 2, 3}, {4, 0, five}}, 1},
          {{{1, 2, 3}, {five, 4, 0}}, -1},
          {{{4, 1, 2}, {five, 0, 3}}, five},
          {{{3, 2, 4}, {1, five, 0}}, 14},
      };
  for (const auto &tc : test_case) {
    EXPECT_EQ(Solution::slidingPuzzle(std::get<0>(tc)), std::get<1>(tc));
  }
}
