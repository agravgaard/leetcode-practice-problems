#include <algorithm>
#include <array>
#include <string>

#include <gtest/gtest.h>

constexpr auto len_alphabet = 26U;

auto disgusting_histogram_solution(const std::string &s)
    -> std::array<size_t, len_alphabet> {
  std::array<size_t, len_alphabet> out{};
  for (const auto &l : s) {
    out.at(l - 'a')++;
  }
  return out;
}

auto count_uneven(const std::array<size_t, len_alphabet> &hist) -> size_t {
  return std::count_if(hist.cbegin(), hist.cend(),
                       [](size_t h) { return h % 2 == 1; });
}

class Solution {
public:
  static auto canConstruct(const std::string &s, int k) -> bool {
    const auto hist = disgusting_histogram_solution(s);
    if (count_uneven(hist) > static_cast<size_t>(k)) {
      return false;
    }
    return s.size() >= static_cast<size_t>(k);
  }
};

TEST(LeetCodeTests, 1400) { // NOLINT
  constexpr auto five = 5;
  constexpr auto six = 6;
  constexpr auto seven = 7;
  constexpr auto twelve = 12;
  // in-string, in-K, expected-out, expected-number-of-uneven
  const std::vector<std::tuple<std::string, int, bool, size_t>> test_cases{
      {"annabelle", 2, true, 1},
      {"leetcode", 4, false, six},
      {"true", 4, true, 4},
      {"yzyzyzyzyzyzyzy", 2, true, 1},
      {"cr", seven, false, 2},
      {"cxayrgpcctwlfupgzirmazszgfiusitvzsnngmivctprcotcuutfxdpbrdlqukhxkrmpw"
       "qq"
       "wdxxrptaftpnilfzcmknqljgbfkzuhksxzplpoozablefndimqnffrqfwgaixsovmmili"
       "cj"
       "whppikryerkdidupvzdmoejzczkbdpfqkgpbxcrxphhnxfazovxbvaxyxhgqxcxirjsry"
       "qx"
       "treptusvupsstylpjniezyfokbowpbgxbtsemzsvqzkbrhkvzyogkuztgfmrprz",
       five, false, twelve},
  };

  for (const auto &tc : test_cases) {
    EXPECT_EQ(count_uneven(disgusting_histogram_solution(std::get<0>(tc))),
              std::get<3>(tc))
        << "Oops, wrong unique count on " << std::get<0>(tc);

    EXPECT_EQ(Solution::canConstruct(std::get<0>(tc), std::get<1>(tc)),
              std::get<2>(tc))
        << "Oops, wrong result on " << std::get<0>(tc);
  }
}
