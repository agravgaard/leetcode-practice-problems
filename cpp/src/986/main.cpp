#include <algorithm>
#include <string>
#include <vector>

#include <gtest/gtest.h>

using pairlist = std::vector<std::vector<int>>;

class Solution {
public:
  static auto intervalIntersection(const pairlist &firstList,
                                   const pairlist &secondList) -> pairlist {
    pairlist output;
    for (const auto &fp : firstList) {
      for (const auto &sp : secondList) {
        if (sp[0] > fp[1]) {
          // stop when we get too far
          break;
        }
        if (sp[1] >= fp[0]) {
          // some overlap ->
          output.push_back({std::max(sp[0], fp[0]), std::min(sp[1], fp[1])});
        }
      }
    }
    return output;
  }
};

TEST(LeetCodeTests, 986) { // NOLINT
  const auto firstList = pairlist{{0, 2}, {5, 10}, {13, 23}, {24, 25}};
  const auto secondList = pairlist{{1, 5}, {8, 12}, {15, 24}, {25, 26}};
  const auto true_output =
      pairlist{{1, 2}, {5, 5}, {8, 10}, {15, 23}, {24, 24}, {25, 25}};

  EXPECT_EQ(Solution::intervalIntersection(firstList, secondList), true_output);
}
