// Definition for a QuadTree node.
class Node {
public:
  bool val = false;
  bool isLeaf = false;
  Node *topLeft = nullptr;
  Node *topRight = nullptr;
  Node *bottomLeft = nullptr;
  Node *bottomRight = nullptr;

  Node(bool _val, bool _isLeaf) {
    val = _val;
    isLeaf = _isLeaf;
  }

  Node(bool _val, bool _isLeaf, Node *_topLeft, Node *_topRight,
       Node *_bottomLeft, Node *_bottomRight) {
    val = _val;
    isLeaf = _isLeaf;
    topLeft = _topLeft;
    topRight = _topRight;
    bottomLeft = _bottomLeft;
    bottomRight = _bottomRight;
  }
};

#include <array>
#include <vector>

using grid_t = std::vector<std::vector<int>>;

class Solution {
public:
  auto construct(const grid_t &grid) -> Node * {
    this->grid = grid;
    this->leafNodes.at(0) =
        new Node(false, true, nullptr, nullptr, nullptr, nullptr);
    this->leafNodes.at(1) =
        new Node(true, true, nullptr, nullptr, nullptr, nullptr);
    return construct(0, 0, static_cast<int>(grid.size()));
  }

  auto construct(int r, int c, int n) -> Node * {
    if (n == 1) {
      return leafNodes.at(grid.at(r).at(c));
    }
    n /= 2;
    // Recurse from top-left corner of each subnode:
    auto *tl = construct(r, c, n);
    auto *tr = construct(r, c + n, n);
    auto *bl = construct(r + n, c, n);
    auto *br = construct(r + n, c + n, n);
    if (tl == tr && tl == bl && tl == br) {
      return tl;
    }
    return new Node(false, false, tl, tr, bl, br);
  }

private:
  grid_t grid;
  std::array<Node *, 2> leafNodes;
};
