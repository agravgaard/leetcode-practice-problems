#include <string>
#include <vector>

#include <gtest/gtest.h>

auto distribute_spaces(std::string *line_p, const size_t maxWidth) -> void {
  auto &line = *line_p;
  if (line.size() == maxWidth) {
    return;
  }
  const auto spaces_to_dist = maxWidth - line.size();
  auto last_insrt = 0U;
  auto round = 0U;
  for (auto i = 0U; i < spaces_to_dist; ++i) {
    if (line.find_first_of(' ') == std::string::npos) {
      for (auto ii = 0U; ii < spaces_to_dist; ++ii) {
        line += " ";
      }
      break;
    }
    for (auto l_i = last_insrt;; ++l_i) {
      if (l_i >= line.size()) {
        // Start over when we reach end
        l_i = 0U;
        ++round;
      }
      if (line[l_i] == ' ') {
        line.insert(l_i + 1, {' '});
        last_insrt = l_i + 2 + round;
        break;
      }
    }
  }
}

class Solution {
public:
  static auto fullJustify(const std::vector<std::string> &words, int maxWidth)
      -> std::vector<std::string> {
    std::vector<std::string> lines;
    auto line_no = 0;
    auto first_in_line = true;
    for (const auto &w : words) {
      if (!first_in_line && lines[line_no].size() + w.size() + 1 >
                                static_cast<size_t>(maxWidth)) {
        distribute_spaces(&lines[line_no], maxWidth);
        line_no++;
        first_in_line = true;
      } else if (!first_in_line) {
        lines[line_no] = lines[line_no] + " " + w;
      }
      if (first_in_line) {
        lines.push_back(w);
        first_in_line = false;
      }
    }
    // For last line, append spaces:
    const auto spaces_to_dist = maxWidth - lines[line_no].size();
    for (auto i = 0U; i < spaces_to_dist; ++i) {
      lines[line_no] += " ";
    }
    return lines;
  }
};

TEST(LeetCodeTests, 68) { // NOLINT

  constexpr auto i16 = 16;
  constexpr auto i20 = 20;
  std::vector<
      std::tuple<std::vector<std::string>, int, std::vector<std::string>>>
      test_cases = {
          {{"This", "is", "an", "example", "of", "text", "justification."},
           i16,
           {"This    is    an", "example  of text", "justification.  "}},
          {{"What", "must", "be", "acknowledgment", "shall", "be"},
           i16,
           {"What   must   be", "acknowledgment  ", "shall be        "}},
          {{"Science", "is", "what", "we", "understand", "well", "enough", "to",
            "explain", "to", "a", "computer.", "Art", "is", "everything",
            "else", "we", "do"},
           i20,
           {"Science  is  what we", "understand      well",
            "enough to explain to", "a  computer.  Art is",
            "everything  else  we", "do                  "}}};

  for (auto &tc : test_cases) {
    EXPECT_EQ(Solution::fullJustify(std::get<0>(tc), std::get<1>(tc)),
              std::get<2>(tc));
    // leet-code will check the solution, here we just check it runs
  }
}
