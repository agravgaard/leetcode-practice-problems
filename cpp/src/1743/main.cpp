#include <algorithm>
#include <deque>
#include <iterator>
#include <queue>
#include <vector>

#include <gtest/gtest.h>

using pairlist = std::vector<std::vector<int>>;

class Solution {
public:
  static auto restoreArray(const pairlist &adjacentPairs) -> std::vector<int> {
    if (adjacentPairs.size() == 1) {
      return adjacentPairs.at(0);
    }
    std::deque<int> deq{adjacentPairs[0][0], adjacentPairs[0][1]};
    std::queue<std::vector<int>> unused;
    for (auto i = 1U; i < adjacentPairs.size(); ++i) {
      const auto ap = adjacentPairs.at(i);
      if (ap[0] == deq.front()) {
        deq.push_front(ap[1]);
      } else if (ap[1] == deq.front()) {
        deq.push_front(ap[0]);
      } else if (ap[0] == deq.back()) {
        deq.push_back(ap[1]);
      } else if (ap[1] == deq.back()) {
        deq.push_back(ap[0]);
      } else {
        unused.push(ap);
      }
    }
    while (!unused.empty()) {
      const auto ap = unused.front();
      if (ap[0] == deq.front()) {
        deq.push_front(ap[1]);
      } else if (ap[1] == deq.front()) {
        deq.push_front(ap[0]);
      } else if (ap[0] == deq.back()) {
        deq.push_back(ap[1]);
      } else if (ap[1] == deq.back()) {
        deq.push_back(ap[0]);
      } else {
        unused.push(ap);
      }
      unused.pop();
    }

    std::vector<int> out(deq.size());
    std::copy(deq.begin(), deq.end(), out.begin());
    return out;
  }
};

TEST(LeetCodeTests, 1743) { // NOLINT
  const auto adjacentPairs = pairlist{{4, -2}, {1, 4}, {-3, 1}};
  const auto true_output = std::vector<int>{-3, 1, 4, -2};

  EXPECT_EQ(Solution::restoreArray(adjacentPairs), true_output);
}
