#include <algorithm>
#include <string>
#include <vector>

using ticket_t = std::vector<std::string>;

auto attempt_route(const ticket_t &start, std::vector<size_t> *visited_ids,
                   const std::vector<ticket_t> &t_vec) -> bool {
  if (visited_ids->size() == t_vec.size()) {
    return true;
  }
  size_t id = 0;
  for (const auto &nt : t_vec) {
    // Match start ticket destination with new ticket beginning
    if (start.at(1) == nt.at(0)) {
      // Make sure we haven't already used this ticket
      if (std::find(visited_ids->cbegin(), visited_ids->cend(), id) ==
          visited_ids->end()) {
        visited_ids->push_back(id);
        if (attempt_route(nt, visited_ids, t_vec)) {
          return true;
        }
        visited_ids->pop_back();
      }
    }
    id++;
  }
  return false;
}

auto construct_route(const std::vector<ticket_t> &t_vec)
    -> std::vector<std::string> {
  std::vector<size_t> v_id;
  size_t id = 0;
  for (const auto &t : t_vec) {
    if (t.at(0) == "JFK") {
      v_id.clear();
      v_id.push_back(id);
      if (attempt_route(t, &v_id, t_vec)) {
        break;
      }
    }
    ++id;
  }

  std::vector<std::string> out(t_vec.size() + 1);
  out.at(0) = t_vec.at(v_id.at(0)).at(0);
  auto i = 1;
  for (auto tid : v_id) {
    out.at(i) = t_vec.at(tid).at(1);
    ++i;
  }

  return out;
}

class Solution {
public:
  static auto findItinerary(std::vector<ticket_t> &tickets)
      -> std::vector<std::string> {
    std::sort(tickets.begin(), tickets.end());
    return construct_route(tickets);
  }
};

#include <gtest/gtest.h>

TEST(LeetCodeTests, 332) { // NOLINT
  std::vector<std::tuple<std::vector<ticket_t>, std::vector<std::string>>>
      test_case = {
          {{{"MUC", "LHR"}, {"JFK", "MUC"}, {"SFO", "SJC"}, {"LHR", "SFO"}},
           {"JFK", "MUC", "LHR", "SFO", "SJC"}},
          {{{"JFK", "SFO"},
            {"JFK", "ATL"},
            {"SFO", "ATL"},
            {"ATL", "JFK"},
            {"ATL", "SFO"}},
           {"JFK", "ATL", "JFK", "SFO", "ATL", "SFO"}}};

  for (auto &tc : test_case) {
    EXPECT_EQ(Solution::findItinerary(std::get<0>(tc)), std::get<1>(tc));
  }
}
