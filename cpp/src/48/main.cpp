#include <vector>

#include <gtest/gtest.h>

using matrix_t = std::vector<std::vector<int>>;

class Solution {
public:
  static void rotate(matrix_t &matrix) {
    // n X n matrix
    const auto n = matrix.size() - 1;
    for (auto layer = 0U; layer <= n / 2; ++layer) {
      for (auto i = layer; i < n - layer; ++i) {
        auto tl = matrix[layer][i];
        auto tr = matrix[i][n - layer];
        auto br = matrix[n - layer][n - i];
        auto bl = matrix[n - i][layer];

        // tr -> bl
        matrix[layer][i] = bl;
        // tr -> tl
        matrix[i][n - layer] = tl;
        // br -> tr
        matrix[n - layer][n - i] = tr;
        // bl -> br
        matrix[n - i][layer] = br;
      }
    }
  }
};

TEST(LeetCodeTests, 48) { // NOLINT
  const auto test_cases = std::vector<std::tuple<matrix_t, matrix_t>>{
      {{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}, {{7, 4, 1}, {8, 5, 2}, {9, 6, 3}}},
      {{{5, 1, 9, 11}, {2, 4, 8, 10}, {13, 3, 6, 7}, {15, 14, 12, 16}},
       {{15, 13, 2, 5}, {14, 3, 4, 1}, {12, 6, 8, 9}, {16, 7, 10, 11}}},
      {{{1}}, {{1}}},
      {{{1, 2}, {3, 4}}, {{3, 1}, {4, 2}}}};

  for (const auto &tc : test_cases) {
    auto mat = std::get<0>(tc);
    Solution::rotate(mat);
    EXPECT_EQ(mat, std::get<1>(tc));
  }
}
