#include <algorithm>
#include <set>
#include <string>
#include <variant>
#include <vector>

#include <gtest/gtest.h>

struct item_t {
  std::string key{};
  int timestamp = 0;
  std::string value{};

  inline auto operator<(const item_t &other) const -> bool {
    // bug in clang-tidy: https://bugs.llvm.org/show_bug.cgi?id=46235
    // NOLINTNEXTLINE(hicpp-use-nullptr, modernize-use-nullptr)
    return key < other.key || (key == other.key && timestamp < other.timestamp);
  }
};

class TimeMap {
public:
  /** Initialize your data structure here. */
  TimeMap() = default;

  // comsume args
  void set(std::string &&key, std::string &&value, const int timestamp) {
    const auto new_item = item_t{std::move(key), timestamp, std::move(value)};
    const auto it = std::lower_bound(data_.cbegin(), data_.cend(), new_item);
    if (it != data_.cend()) {
      data_.insert(it, new_item);
    } else {
      data_.push_back(new_item);
    }
  }

  [[nodiscard]] auto get(const std::string &key, const int timestamp) const
      -> const std::string & {
    if (data_.empty()) {
      return empty_str;
    }
    const auto vkey = item_t{key, timestamp, empty_str};
    auto it = std::upper_bound(data_.begin(), data_.end(), vkey);
    if (it != data_.begin()) {
      --it;
    }
    if (it->key == key && it->timestamp <= timestamp) {
      return it->value;
    }
    return empty_str;
  }

private:
  std::vector<item_t> data_{};
  const std::string empty_str{};
};

/**
 * Your TimeMap object will be instantiated and called as such:
 * TimeMap* obj = new TimeMap();
 * obj->set(key,value,timestamp);
 * string param_2 = obj->get(key,timestamp);
 */
TEST(LeetCodeTests, 981) { // NOLINT
  const auto test_cases = std::vector<
      std::tuple<std::vector<std::string>,
                 std::vector<std::vector<std::variant<std::string, int>>>,
                 std::vector<std::string>>>{
      {{"TimeMap", "set", "get", "get", "set", "get", "get"},
       {{},
        {"foo", "bar", 1},
        {"foo", 1},
        {"foo", 3},
        {"foo", "bar2", 4},
        {"foo", 4},
        {"foo", 5}},
       {"", "", "bar", "bar", "", "bar2", "bar2"}},
      {{"TimeMap", "set", "set", "get", "get", "get", "get", "get"},
       {{},
        {"love", "high", 10},
        {"love", "low", 20},
        {"love", 5},
        {"love", 10},
        {"love", 15},
        {"love", 20},
        {"love", 25}},
       {"", "", "", "", "high", "high", "low", "low"}}};

  for (const auto &test : test_cases) {
    auto tm = std::make_unique<TimeMap>();
    for (auto i = 0U; i < std::get<0>(test).size(); ++i) {
      const auto &cmd = std::get<0>(test)[i];
      const auto &input = std::get<1>(test)[i];
      if (cmd == "set") {
        auto i_key = std::get<std::string>(input[0]);
        auto i_value = std::get<std::string>(input[1]);
        auto i_timestamp = std::get<int>(input[2]);
        tm->set(std::move(i_key), std::move(i_value), i_timestamp);
      } else if (cmd == "get") {
        const auto &expect = std::get<2>(test)[i];
        const auto &i_key = std::get<std::string>(input[0]);
        const auto i_timestamp = std::get<int>(input[1]);
        EXPECT_EQ(tm->get(i_key, i_timestamp), expect);
      }
    }
  }
}
