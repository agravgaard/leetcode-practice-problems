#include <algorithm>
#include <stack>
#include <string>
#include <vector>

#include <gtest/gtest.h>

// Binary trees are already defined with this interface:
template <typename T> struct Tree {
  explicit Tree(const T &v) : value(v), left(nullptr), right(nullptr) {}
  T value;
  Tree *left;
  Tree *right;
};

class Solution {
public:
  static auto kthSmallestInBST(Tree<int> *t, int k) -> int {
    std::stack<Tree<int> *> path;
    Tree<int> *tmp = nullptr;
    tmp = t;

    while (tmp != nullptr) {
      // save parent
      path.push(tmp);
      tmp = tmp->left;
    }

    auto count = 0;

    while (true) {
      tmp = path.top();
      path.pop();
      ++count;
      if (count == k) {
        break;
      }

      if (tmp->right != nullptr) {
        tmp = tmp->right;
        while (tmp != nullptr) {
          path.push(tmp);
          tmp = tmp->left;
        }
      }
    }
    return tmp->value;
  }
};

TEST(LeetCodeTests, 230) { // NOLINT
  auto test_t = Tree(3);
  auto one = Tree(1);
  auto two = Tree(2);
  auto four = Tree(4);
  test_t.left = &one;
  test_t.left->right = &two;
  test_t.right = &four;

  constexpr auto test_k = 4;

  constexpr auto true_val = 4;

  EXPECT_EQ(Solution::kthSmallestInBST(&test_t, test_k), true_val);
}
