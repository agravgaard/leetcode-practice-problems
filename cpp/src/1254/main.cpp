#include <algorithm>
#include <array>
#include <vector>

#include <gtest/gtest.h>


using grid_t = std::vector<std::vector<int>>;
using coord_t = std::array<size_t, 2>;

auto grow_island(grid_t& grid, const coord_t &seed, int id) -> bool {
  auto is_closed = true;
  auto new_coords = std::vector<coord_t>{seed};
  std::vector<coord_t> tmp_new_coords{};
  do {
    for (const auto &c : new_coords) {
      const auto y = c[0];
      const auto x = c[1];
      grid[y][x] = id;
      // left
      if (x > 0 && grid[y][x-1] == 0){
        tmp_new_coords.push_back({y, x-1});
      }
      // right
      if (x+1 < grid[y].size() && grid[y][x+1] == 0){
        tmp_new_coords.push_back({y, x+1});
      }
      // up
      if (y > 0 && grid[y-1][x] == 0){
        tmp_new_coords.push_back({y-1, x});
      }
      // down
      if (y+1 < grid.size() && grid[y+1][x] == 0){
        tmp_new_coords.push_back({y+1, x});
      }
      // is any touching the edge?
      if (is_closed && (y == 0 || y+1 == grid.size() || x == 0 || x+1 == grid[y].size())) {
        is_closed = false;
      }
    }
    new_coords.clear();
    std::copy(tmp_new_coords.begin(), tmp_new_coords.end(), std::back_inserter(new_coords));
    tmp_new_coords.clear();

  } while (!new_coords.empty());

  return is_closed;
}

class Solution {
public:
    static auto closedIsland(grid_t& grid) -> int {
      auto closed_islands = 0U;

      auto row_start = size_t{0};
      for (auto island_id = 2; /*break out*/ ; ++island_id){
        coord_t seed{};
        const auto it_row = std::find_if(grid.cbegin() + row_start, grid.cend(),
          [i_row = row_start, &seed](const auto &row) mutable {
            const auto it = std::find(row.cbegin(), row.cend(), 0);
            if (it != row.cend()) {
              seed = {i_row, static_cast<size_t>(std::distance(row.cbegin(), it))};
              return true;
            }
            ++i_row;
            return false;
          });

        if (it_row != grid.cend()){
          row_start = static_cast<size_t>(std::distance(grid.cbegin(), it_row));

          if (grow_island(grid, seed, island_id)) {
            ++closed_islands;
          }
        } else {
          break;
        }
      }
      return static_cast<int>(closed_islands);

    }
};

TEST(LeetCodeTests, 1254) { // NOLINT
  auto test_cases = std::vector<std::tuple<grid_t, int>>{
      { {{1,1,1,1,1,1,1,0},{1,0,0,0,0,1,1,0},{1,0,1,0,1,1,1,0},{1,0,0,0,0,1,0,1},{1,1,1,1,1,1,1,0}}, 2},
          {{{0,0,1,0,0},{0,1,0,1,0},{0,1,1,1,0}}, 1},
              {{{1,1,1,1,1,1,1},{1,0,0,0,0,0,1},{1,0,1,1,1,0,1},{1,0,1,0,1,0,1},{1,0,1,1,1,0,1},{1,0,0,0,0,0,1},{1,1,1,1,1,1,1}}, 2}
  };

  for (auto &tc : test_cases) {
    EXPECT_EQ(Solution::closedIsland(std::get<0>(tc)), std::get<1>(tc));
  }
}
