#include <algorithm>
#include <string>
#include <vector>

#include <gtest/gtest.h>

class Solution {
public:
  static auto minIncrementForUnique(const std::vector<int> &cnums) -> int {
    if (cnums.size() < 2) {
      return 0;
    }
    auto nums = cnums;
    std::sort(nums.begin(), nums.end());
    size_t count = 0;
    for (auto i = 1U; i < nums.size(); ++i) {
      if (nums[i - 1] >= nums[i]) {
        auto diff = nums[i - 1] - nums[i] + 1;
        nums[i] += diff;
        count += diff;
      }
    }
    return static_cast<int>(count);
  }
};

TEST(LeetCodeTests, 945) { // NOLINT
  constexpr auto seven = 7;
  const auto nums = {3, 2, 1, 2, 1, seven};
  constexpr auto true_output = 6;

  EXPECT_EQ(Solution::minIncrementForUnique(nums), true_output);
}
