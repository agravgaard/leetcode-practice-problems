#include <algorithm>
#include <map>
#include <memory>
#include <numeric>
#include <vector>

#include <gtest/gtest.h>

class LRUCache {
public:
  explicit LRUCache(int capacity) {
    cache_.resize(capacity);
    const auto init_pair = std::make_pair(-1, -1);
    std::fill(cache_.begin(), cache_.end(), init_pair);
    lru_.resize(capacity);
    std::iota(lru_.begin(), lru_.end(), 0U);
  }

  auto get(int key) -> int {
    auto index = 0U;
    for (auto &[k, v] : cache_) {
      if (key == k) {
        // move index to end of lru_
        auto it_index = std::find(lru_.begin(), lru_.end(), index);
        std::rotate(it_index, it_index + 1, lru_.end());
        return v;
      }
      index++;
    }
    return -1;
  }

  void put(int key, int value) {
    auto index = 0U;
    for (auto &c : cache_) {
      if (key == c.first) {
        auto it_index = std::find(lru_.begin(), lru_.end(), index);
        std::rotate(it_index, it_index + 1, lru_.end());
        c.second = value;
        return;
      }
      index++;
    }
    cache_[lru_[0]] = std::make_pair(key, value);
    std::rotate(lru_.begin(), lru_.begin() + 1, lru_.end());
  }

private:
  std::vector<size_t> lru_{};
  std::vector<std::pair<int, int>> cache_{};
};

TEST(LeetCodeTests, 46) { // NOLINT
  /** (From LeetCode:)
   * Your LRUCache object will be instantiated and called as such:
   * LRUCache* obj = new LRUCache(capacity);
   * int param_1 = obj->get(key);
   * obj->put(key,value);
   */
  const auto capacity = 2;
  auto obj = std::make_unique<LRUCache>(capacity);
  obj->put(1, 1);
  obj->put(2, 2);
  EXPECT_EQ(obj->get(1), 1);
  obj->put(3, 3);
  EXPECT_EQ(obj->get(2), -1);
  obj->put(4, 4);
  EXPECT_EQ(obj->get(1), -1);
  EXPECT_EQ(obj->get(3), 3);
  EXPECT_EQ(obj->get(4), 4);
}
