#include <algorithm>
#include <array>
#include <cmath>
#include <set>
#include <vector>

#include <gtest/gtest.h>

using grid_t = std::vector<std::vector<int>>;
using coord_t = std::array<size_t, 2>;
using coords_t = std::set<coord_t>;

auto find_new_neighbours(grid_t &grid, coords_t &new_coords) -> coords_t {
  // we paint grid and erase inner coords

  coords_t tmp_new_coords;
  for (auto c = new_coords.begin(); c != new_coords.end();) {
    auto is_outer = true;
    const auto y = (*c)[0];
    const auto x = (*c)[1];
    // up
    if (y > 0U && grid[y - 1][x] == 1) {
      tmp_new_coords.insert({y - 1, x});
    } else if (y > 0U && grid[y - 1][x] == 0) {
      is_outer = true;
    }
    // down
    if (y < grid.size() - 1 && grid[y + 1][x] == 1) {
      tmp_new_coords.insert({y + 1, x});
    } else if (!is_outer && y < grid.size() - 1 && grid[y + 1][x] == 0) {
      is_outer = true;
    }
    // left
    if (x > 0U && grid[y][x - 1] == 1) {
      tmp_new_coords.insert({y, x - 1});
    } else if (!is_outer && x > 0U && grid[y][x - 1] == 0) {
      is_outer = true;
    }
    // right
    if (x < grid[0].size() - 1 && grid[y][x + 1] == 1) {
      tmp_new_coords.insert({y, x + 1});
    } else if (!is_outer && x < grid[0].size() - 1 && grid[y][x + 1] == 0) {
      is_outer = true;
    }
    grid[y][x] = 2;
    if (!is_outer) {
      c = new_coords.erase(c);
    } else {
      ++c;
    }
  }
  return tmp_new_coords;
}
auto grow_from_seed(grid_t &grid, const coord_t &seed) -> coords_t {
  auto new_coords = coords_t{seed};

  coords_t out;
  auto prev_size = size_t{0};
  do {
    prev_size = out.size();

    auto tmp_new_coords = find_new_neighbours(grid, new_coords);
#ifdef __apple_build_version__
    // alternative to merge
    out.insert(new_coords.begin(), new_coords.end());
#else
    out.merge(new_coords);
#endif
    new_coords = std::move(tmp_new_coords);
  } while (out.size() != prev_size);

  return out;
}

auto grow_island(grid_t &grid, const coords_t &other_island) -> coords_t {
  coord_t seed;
  // 2D find_first_of:
  auto i = 0U;
  auto seed_found = false;
  for (const auto &row : grid) {
    auto j = 0U;
    for (const auto &s : row) {
      if (s == 1 && other_island.find(coord_t{i, j}) == other_island.cend()) {
        seed = {i, j};
        seed_found = true;
        break;
      }
      ++j;
    }
    if (seed_found) {
      break;
    }
    ++i;
  }

  return grow_from_seed(grid, seed);
}

constexpr auto absdiff(size_t a, size_t b) -> size_t {
  return a > b ? a - b : b - a;
}

auto minimal_manhattan(const coords_t &c_a, const coords_t &c_b) -> size_t {
  auto min_dist = std::numeric_limits<size_t>::max();
  for (const auto &a : c_a) {
    auto start_b = c_b.lower_bound(a);
    for (auto it = start_b; it != c_b.end(); ++it) {
      const auto b = *it;
      const auto y_dist = absdiff(a[0], b[0]);
      if (y_dist > min_dist) {
        break;
      }
      const auto x_dist = absdiff(a[1], b[1]);
      min_dist = std::min(min_dist, x_dist + y_dist);
    }
    if (start_b != c_b.begin()) {
      for (auto it = std::make_reverse_iterator(start_b); it != c_b.rend();
           ++it) {
        const auto b = *it;
        const auto y_dist = absdiff(a[0], b[0]);
        if (y_dist > min_dist) {
          break;
        }
        const auto x_dist = absdiff(a[1], b[1]);
        min_dist = std::min(min_dist, x_dist + y_dist);
      }
    }
  }
  return min_dist - 1;
}

class Solution {
public:
  static auto shortestBridge(grid_t &grid) -> int {
    coords_t second_island;
    const auto first_island = grow_island(grid, second_island);
    second_island = grow_island(grid, first_island);
    // Output int only because leetcode wants it
    return static_cast<int>(minimal_manhattan(first_island, second_island));
  }
};

TEST(LeetCodeTests, 934) { // NOLINT
  auto test_cases = std::vector<std::tuple<grid_t, int>>{
      {{{0, 1}, {1, 0}}, 1},
      {{{0, 1, 0}, {0, 0, 0}, {0, 0, 1}}, 2},
      {{{1, 1, 1, 1, 1},
        {1, 0, 0, 0, 1},
        {1, 0, 1, 0, 1},
        {1, 0, 0, 0, 1},
        {1, 1, 1, 1, 1}},
       1}};

  for (auto &tc : test_cases) {
    EXPECT_EQ(Solution::shortestBridge(std::get<0>(tc)), std::get<1>(tc));
  }
}
