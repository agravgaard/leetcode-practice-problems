#include <algorithm>
#include <cmath>
#include <numeric>
#include <vector>

#include <gtest/gtest.h>

template <typename T> struct point_t {
  T x;
  T y;
};

template <typename T>
constexpr auto operator+(const point_t<T> lhs, const point_t<T> rhs)
    -> point_t<T> {
  return {lhs.x + rhs.x, lhs.y + rhs.y};
}
template <typename T>
constexpr auto operator+(const point_t<T> lhs, const T rhs) -> point_t<T> {
  return {lhs.x + rhs, lhs.y + rhs};
}
template <typename T>
constexpr auto operator*(const point_t<T> lhs, const T rhs) -> point_t<T> {
  return {lhs.x * rhs, lhs.y * rhs};
}

template <typename T, typename U>
constexpr auto operator/(const point_t<U> lhs, const T rhs) -> point_t<T> {
  return {static_cast<T>(lhs.x) / rhs, static_cast<T>(lhs.y) / rhs};
}

auto dist_fn(point_t<double> centre, point_t<int> p) -> double {
  return std::sqrt(std::pow(centre.x - p.x, 2) + std::pow(centre.y - p.y, 2));
}

auto to_point(const std::vector<int> &p) -> point_t<int> {
  return point_t<int>{p.at(0), p.at(1)};
}

auto get_start(const std::vector<point_t<int>> &points) -> point_t<double> {
  point_t<double> mean_p{0, 0};
  mean_p = std::accumulate(points.cbegin(), points.cend(), mean_p,
                           [](auto a, auto b) {
                             return point_t<double>{b.x + a.x, b.y + a.y};
                           });
  return {mean_p.x / points.size(), mean_p.y / points.size()};
}

auto get_dist(const point_t<double> &p_centre,
              const std::vector<point_t<int>> &p_vec) -> double {
  return std::accumulate(
      p_vec.cbegin(), p_vec.cend(), 0.0,
      [p_centre](auto a, auto b) { return a + dist_fn(p_centre, b); });
}

enum struct direction_t {
  UP,
  DOWN,
  LEFT,
  RIGHT,
  ZERO,
};
constexpr auto n_up = point_t<double>{0.0, 1.0};
constexpr auto n_down = point_t<double>{0.0, -1.0};
constexpr auto n_left = point_t<double>{-1.0, 0.0};
constexpr auto n_right = point_t<double>{1.0, 0.0};

auto best_step(const point_t<double> p_centre,
               const std::vector<point_t<int>> &p_vec, double step_scale,
               double cur_dist) -> direction_t {
  const auto j_up = get_dist(p_centre + (n_up * step_scale), p_vec);

  const auto j_down = get_dist(p_centre + (n_down * step_scale), p_vec);

  const auto j_left = get_dist(p_centre + (n_left * step_scale), p_vec);

  const auto j_right = get_dist(p_centre + (n_right * step_scale), p_vec);

  if (j_up < std::min({j_down, j_left, j_right, cur_dist})) {
    return direction_t::UP;
  }
  if (j_down < std::min({j_up, j_left, j_right, cur_dist})) {
    return direction_t::DOWN;
  }
  if (j_right < std::min({j_down, j_left, j_up, cur_dist})) {
    return direction_t::RIGHT;
  }
  if (j_left < std::min({j_down, j_right, j_up, cur_dist})) {
    return direction_t::LEFT;
  }
  return direction_t::ZERO;
}

auto optimize(point_t<double> *p_centre,
              const std::vector<point_t<int>> &p_vec) {
  auto &p_c = *p_centre;
  auto scale = 1.0;
  constexpr auto tol = 0.00001;
  constexpr auto scale_factor = 0.5;
  while (true) {
    if (scale < tol) {
      break;
    }
    const auto cur_dist = get_dist(p_c, p_vec);
    const auto dir = best_step(p_c, p_vec, scale, cur_dist);
    switch (dir) {
    case direction_t::UP:
      p_c = p_c + (n_up * scale);
      break;
    case direction_t::DOWN:
      p_c = p_c + (n_down * scale);
      break;
    case direction_t::LEFT:
      p_c = p_c + (n_left * scale);
      break;
    case direction_t::RIGHT:
      p_c = p_c + (n_right * scale);
      break;
    default:
      scale *= scale_factor;
      break;
    }
  }
}

class Solution {
public:
  static auto getMinDistSum(const std::vector<std::vector<int>> &positions)
      -> double {
    std::vector<point_t<int>> p_vec;
    std::transform(positions.cbegin(), positions.cend(),
                   std::back_inserter(p_vec), to_point);

    auto p_centre = get_start(p_vec);
    optimize(&p_centre, p_vec);

    const auto dist = get_dist(p_centre, p_vec);

    return dist;
  }
};

TEST(LeetCodeTests, 1515) { // NOLINT
  const std::vector<std::tuple<std::vector<std::vector<int>>, double>>
      test_case = {
          {{{44, 23},
            {18, 45},
            {6, 73},
            {0, 76},
            {10, 50},
            {30, 7},
            {92, 59},
            {44, 59},
            {79, 45},
            {69, 37},
            {66, 63},
            {10, 78},
            {88, 80},
            {44, 87}},
           499.28078},
      };

  for (const auto &tc : test_case) {
    EXPECT_NEAR(Solution::getMinDistSum(std::get<0>(tc)), std::get<1>(tc), 1e-5);
  }
}
