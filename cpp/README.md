

## For Roku:
 * [Morris binary tree traversal](https://leetcode.com/problems/binary-tree-inorder-traversal/)
 * [Subarray sum equals K](https://leetcode.com/problems/subarray-sum-equals-k/)
 * [Design hashmap](https://leetcode.com/problems/design-hashmap/)

