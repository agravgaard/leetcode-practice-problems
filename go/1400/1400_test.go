package main

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMain(m *testing.M) {
	os.Exit(m.Run())
}

type testCase struct {
	testString string
	testK      int
	trueUneven uint
	trueUnique int
	trueResult bool
}

var testCases = []testCase{
	{"annabelle", 2, 1, 5, true},
	{"leetcode", 3, 6, 6, false},
	{"true", 4, 4, 4, true},
	{"yzyzyzyzyzyzyzy", 2, 1, 2, true},
	{"cr", 7, 2, 2, false},
	{"aaa", 2, 1, 1, true},
	{"qlkzenwmmnpkopu", 15, 7, 11, true},
}

func TestCountUneven(t *testing.T) {
	for _, tc := range testCases {
		n_uneven_test, n_unique_test := countUnevenAndUniqueChars(tc.testString)
		assert.Equal(t, tc.trueUneven, n_uneven_test)
		assert.Equal(t, tc.trueUnique, n_unique_test)
	}
}

func TestCanConstruct(t *testing.T) {
	for _, tc := range testCases {
		test_result := canConstruct(tc.testString, tc.testK)
		assert.Equal(t, tc.trueResult, test_result, tc.testString)
	}
}
