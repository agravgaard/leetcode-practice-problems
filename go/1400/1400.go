package main

func countUnevenAndUniqueChars(s string) (uint, int) {
	count_map := make(map[rune]uint)
	for _, c := range s {
		count_map[c]++
	}
	count_uneven := uint(0)
	for _, val := range count_map {
		if val%2 != 0 {
			count_uneven++
		}
	}
	return count_uneven, len(count_map)
}

func canConstruct(s string, k int) bool {
	uneven, unique := countUnevenAndUniqueChars(s)
	if k >= unique && len(s) >= k {
		return true
	}
	if k > unique || uint(k) < uneven {
		return false
	}
	return true
}
