# Go implementation of my leetcode solutions

Medium from the list below.

| ID  | Title | Acceptance | Difficulty |
| --- | ----- | ---------- | ---------- |
| 68 | [Text Justification](https://leetcode.com/problems/text-justification) | 30.3% | Hard |
| 1400 | [Construct K Palindrome Strings](https://leetcode.com/problems/construct-k-palindrome-strings) | 63.0% | Medium |
| 773 | [Sliding Puzzle](https://leetcode.com/problems/sliding-puzzle) | 61.2% | Hard |
| 1515 | [Best Position for a Service Centre](https://leetcode.com/problems/best-position-for-a-service-centre) | 38.4% | Hard |
| 427 | [Construct Quad Tree](https://leetcode.com/problems/construct-quad-tree) | 62.8% | Medium |
| 529 | [Minesweeper](https://leetcode.com/problems/minesweeper) | 61.5% | Medium |
| 655 | [Print Binary Tree](https://leetcode.com/problems/print-binary-tree) | 56.4% | Medium |
| 332 | [Reconstruct Itinerary](https://leetcode.com/problems/reconstruct-itinerary) | 38.2% | Medium |
| 1052 | [Grumpy Bookstore Owner](https://leetcode.com/problems/grumpy-bookstore-owner) | 56.0% | Medium |
| 757 | [Set Intersection Size At Least Two](https://leetcode.com/problems/set-intersection-size-at-least-two) | 42.6% | Hard |
| 1696 | [Jump Game VI](https://leetcode.com/problems/jump-game-vi) | 51.5% | Medium |
| 986 | [Interval List Intersections](https://leetcode.com/problems/interval-list-intersections) | 68.5% | Medium |
| 465 | [Optimal Account Balancing](https://leetcode.com/problems/optimal-account-balancing) | 48.5% | Hard |
| 945 | [Minimum Increment to Make Array Unique](https://leetcode.com/problems/minimum-increment-to-make-array-unique) | 46.9% | Medium |
| 1743 | [Restore the Array From Adjacent Pairs](https://leetcode.com/problems/restore-the-array-from-adjacent-pairs) | 63.3% | Medium |
| 1626 | [Best Team With No Conflicts](https://leetcode.com/problems/best-team-with-no-conflicts) | 38.5% | Medium |
| 399 | [Evaluate Division](https://leetcode.com/problems/evaluate-division) | 54.7% | Medium |
| 659 | [Split Array into Consecutive Subsequences](https://leetcode.com/problems/split-array-into-consecutive-subsequences) | 44.5% | Medium |
| 126 | [Word Ladder II](https://leetcode.com/problems/word-ladder-ii) | 23.8% | Hard |
| 200 | [Number of Islands](https://leetcode.com/problems/number-of-islands) | 49.6% | Medium |
| 297 | [Serialize and Deserialize Binary Tree](https://leetcode.com/problems/serialize-and-deserialize-binary-tree) | 50.3% | Hard |
| 212 | [Word Search II](https://leetcode.com/problems/word-search-ii) | 37.5% | Hard |
| 815 | [Bus Routes](https://leetcode.com/problems/bus-routes) | 43.6% | Hard |
| 48 | [Rotate Image](https://leetcode.com/problems/rotate-image) | 60.6% | Medium |
| 340 | [Longest Substring with At Most K Distinct Characters](https://leetcode.com/problems/longest-substring-with-at-most-k-distinct-characters) | 45.7% | Medium |
| 1668 | [Maximum Repeating Substring](https://leetcode.com/problems/maximum-repeating-substring) | 38.6% | Easy |
| 146 | [LRU Cache](https://leetcode.com/problems/lru-cache) | 36.3% | Medium |
| 1135 | [Connecting Cities With Minimum Cost](https://leetcode.com/problems/connecting-cities-with-minimum-cost) | 59.6% | Medium |
| 49 | [Group Anagrams](https://leetcode.com/problems/group-anagrams) | 59.8% | Medium |
| 1352 | [Product of the Last K Numbers](https://leetcode.com/problems/product-of-the-last-k-numbers) | 45.1% | Medium |
| 56 | [Merge Intervals](https://leetcode.com/problems/merge-intervals) | 41.4% | Medium |
| 1640 | [Check Array Formation Through Concatenation](https://leetcode.com/problems/check-array-formation-through-concatenation) | 59.8% | Easy |
| 981 | [Time Based Key-Value Store](https://leetcode.com/problems/time-based-key-value-store) | 54.2% | Medium |
| 646 | [Maximum Length of Pair Chain](https://leetcode.com/problems/maximum-length-of-pair-chain) | 53.2% | Medium |
| 942 | [DI String Match](https://leetcode.com/problems/di-string-match) | 73.8% | Easy |
| 253 | [Meeting Rooms II](https://leetcode.com/problems/meeting-rooms-ii) | 47.2% | Medium |
| 934 | [Shortest Bridge](https://leetcode.com/problems/shortest-bridge) | 50.0% | Medium |
| 224 | [Basic Calculator](https://leetcode.com/problems/basic-calculator) | 38.3% | Hard |
| 1027 | [Longest Arithmetic Subsequence](https://leetcode.com/problems/longest-arithmetic-subsequence) | 49.7% | Medium |
| 977 | [Squares of a Sorted Array](https://leetcode.com/problems/squares-of-a-sorted-array) | 71.8% | Easy |
| 127 | [Word Ladder](https://leetcode.com/problems/word-ladder) | 32.2% | Hard |
| 262 | [Trips and Users](https://leetcode.com/problems/trips-and-users) | 35.6% | Hard |
| 1200 | [Minimum Absolute Difference](https://leetcode.com/problems/minimum-absolute-difference) | 67.1% | Easy |
| 678 | [Valid Parenthesis String](https://leetcode.com/problems/valid-parenthesis-string) | 31.8% | Medium |
| 34 | [Find First and Last Position of Element in Sorted Array](https://leetcode.com/problems/find-first-and-last-position-of-element-in-sorted-array) | 37.5% | Medium |
| 99 | [Recover Binary Search Tree](https://leetcode.com/problems/recover-binary-search-tree) | 42.8% | Hard |
| 1254 | [Number of Closed Islands](https://leetcode.com/problems/number-of-closed-islands) | 61.9% | Medium |
| 218 | [The Skyline Problem](https://leetcode.com/problems/the-skyline-problem) | 36.7% | Hard |
| 188 | [Best Time to Buy and Sell Stock IV](https://leetcode.com/problems/best-time-to-buy-and-sell-stock-iv) | 30.1% | Hard |
| 78 | [Subsets](https://leetcode.com/problems/subsets) | 65.7% | Medium |
| 185 | [Department Top Three Salaries](https://leetcode.com/problems/department-top-three-salaries) | 40.0% | Hard |
| 36 | [Valid Sudoku](https://leetcode.com/problems/valid-sudoku) | 50.9% | Medium |
| 767 | [Reorganize String](https://leetcode.com/problems/reorganize-string) | 50.3% | Medium |
| 706 | [Design HashMap](https://leetcode.com/problems/design-hashmap) | 64.1% | Easy |
| 76 | [Minimum Window Substring](https://leetcode.com/problems/minimum-window-substring) | 36.2% | Hard |
| 1 | [Two Sum](https://leetcode.com/problems/two-sum) | 46.7% | Easy |
| 130 | [Surrounded Regions](https://leetcode.com/problems/surrounded-regions) | 29.8% | Medium |
| 1281 | [Subtract the Product and Sum of Digits of an Integer](https://leetcode.com/problems/subtract-the-product-and-sum-of-digits-of-an-integer) | 85.6% | Easy |
| 206 | [Reverse Linked List](https://leetcode.com/problems/reverse-linked-list) | 65.8% | Easy |
| 763 | [Partition Labels](https://leetcode.com/problems/partition-labels) | 78.1% | Medium |
| 227 | [Basic Calculator II](https://leetcode.com/problems/basic-calculator-ii) | 38.8% | Medium |
| 69 | [Sqrt(x)](https://leetcode.com/problems/sqrtx) | 35.4% | Easy |
| 354 | [Russian Doll Envelopes](https://leetcode.com/problems/russian-doll-envelopes) | 37.7% | Hard |
| 322 | [Coin Change](https://leetcode.com/problems/coin-change) | 37.8% | Medium |
| 17 | [Letter Combinations of a Phone Number](https://leetcode.com/problems/letter-combinations-of-a-phone-number) | 49.9% | Medium |
| 133 | [Clone Graph](https://leetcode.com/problems/clone-graph) | 39.9% | Medium |
| 238 | [Product of Array Except Self](https://leetcode.com/problems/product-of-array-except-self) | 61.4% | Medium |
| 295 | [Find Median from Data Stream](https://leetcode.com/problems/find-median-from-data-stream) | 47.5% | Hard |
| 121 | [Best Time to Buy and Sell Stock](https://leetcode.com/problems/best-time-to-buy-and-sell-stock) | 51.9% | Easy |
| 41 | [First Missing Positive](https://leetcode.com/problems/first-missing-positive) | 34.0% | Hard |
| 415 | [Add Strings](https://leetcode.com/problems/add-strings) | 48.6% | Easy |
| 91 | [Decode Ways](https://leetcode.com/problems/decode-ways) | 27.0% | Medium |
| 973 | [K Closest Points to Origin](https://leetcode.com/problems/k-closest-points-to-origin) | 64.7% | Medium |
| 39 | [Combination Sum](https://leetcode.com/problems/combination-sum) | 59.8% | Medium |
| 21 | [Merge Two Sorted Lists](https://leetcode.com/problems/merge-two-sorted-lists) | 56.5% | Easy |
| 23 | [Merge k Sorted Lists](https://leetcode.com/problems/merge-k-sorted-lists) | 43.1% | Hard |
| 236 | [Lowest Common Ancestor of a Binary Tree](https://leetcode.com/problems/lowest-common-ancestor-of-a-binary-tree) | 49.4% | Medium |
| 122 | [Best Time to Buy and Sell Stock II](https://leetcode.com/problems/best-time-to-buy-and-sell-stock-ii) | 58.9% | Easy |
| 2 | [Add Two Numbers](https://leetcode.com/problems/add-two-numbers) | 35.8% | Medium |
| 102 | [Binary Tree Level Order Traversal](https://leetcode.com/problems/binary-tree-level-order-traversal) | 57.0% | Medium |
| 101 | [Symmetric Tree](https://leetcode.com/problems/symmetric-tree) | 48.5% | Easy |
| 283 | [Move Zeroes](https://leetcode.com/problems/move-zeroes) | 58.7% | Easy |
| 13 | [Roman to Integer](https://leetcode.com/problems/roman-to-integer) | 57.1% | Easy |
| 70 | [Climbing Stairs](https://leetcode.com/problems/climbing-stairs) | 48.8% | Easy |
| 4 | [Median of Two Sorted Arrays](https://leetcode.com/problems/median-of-two-sorted-arrays) | 31.5% | Hard |
| 98 | [Validate Binary Search Tree](https://leetcode.com/problems/validate-binary-search-tree) | 29.0% | Medium |
| 7 | [Reverse Integer](https://leetcode.com/problems/reverse-integer) | 26.0% | Easy |
| 234 | [Palindrome Linked List](https://leetcode.com/problems/palindrome-linked-list) | 42.1% | Easy |
| 3 | [Longest Substring Without Repeating Characters](https://leetcode.com/problems/longest-substring-without-repeating-characters) | 31.6% | Medium |
