package main

import "sort"

func FindSetRepresentatives(sr []int, x int) int {
	// sr[x] -> parent of x
	if sr[x] != x {
		sr[x] = FindSetRepresentatives(sr, sr[x])
		return sr[x]
	} else {
		return x
	}
}

func swapLexOrder(str string, pairs [][]int) string {
	if len(pairs) < 1 || len(str) < 2 {
		return str
	}
	sr := make([]int, len(str))
	for i := range sr {
		sr[i] = i
	}
	// Find all set representatives
	for _, p := range pairs {
		i := FindSetRepresentatives(sr, p[0]-1)
		j := FindSetRepresentatives(sr, p[1]-1)
		if i != j {
			sr[j] = i
		}
	}
	// Create the disjoint sets
	ds := make([][]int, len(str))
	for i := range ds {
		i_sr := FindSetRepresentatives(sr, i)
		ds[i_sr] = append(ds[i_sr], i)
	}
	new_str := []byte(str)
	for _, set := range ds {
		// Create a string from the set
		var rstr []byte
		for _, pos := range set {
			rstr = append(rstr, str[pos])
		}
		// sort the set
		sort.Slice(rstr, func(i, j int) bool { return rstr[i] > rstr[j] })
		// put the sorted set back in the string
		for i, pos := range set {
			new_str[pos] = rstr[i]
		}
	}

	return string(new_str)
}
