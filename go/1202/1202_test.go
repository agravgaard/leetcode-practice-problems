package main

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMain(m *testing.M) {
	os.Exit(m.Run())
}

func TestSwapLexOrder(t *testing.T) {
	test_str := "lvvyfrbhgiyexoirhunnuejzhesylojwbyatfkrv"
	test_pairs := [][]int{
		{13, 23},
		{13, 28},
		{15, 20},
		{24, 29},
		{6, 7},
		{3, 4},
		{21, 30},
		{2, 13},
		{12, 15},
		{19, 23},
		{10, 19},
		{13, 14},
		{6, 16},
		{17, 25},
		{6, 21},
		{17, 26},
		{5, 6},
		{12, 24},
	}

	test_result := swapLexOrder(test_str, test_pairs)

	true_result := "lyyvurrhgxyzvonohunlfejihesiebjwbyatfkrv"
	assert.Equal(t, true_result, test_result)
}

func TestSwapLexOrderSmall(t *testing.T) {
	test_str := "abdc"
	test_pairs := [][]int{
		{1, 4},
		{3, 4},
	}

	test_result := swapLexOrder(test_str, test_pairs)

	true_result := "dbca"
	assert.Equal(t, true_result, test_result)
}
