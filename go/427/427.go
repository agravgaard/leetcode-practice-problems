package main

// Definition for a QuadTree node.
type Node struct {
	Val         bool
	IsLeaf      bool
	TopLeft     *Node
	TopRight    *Node
	BottomLeft  *Node
	BottomRight *Node
}

var oneNode = Node{true, true, nil, nil, nil, nil}
var zeroNode = Node{false, true, nil, nil, nil, nil}

func construct(grid [][]int) *Node {
	var node_out Node

	n := len(grid)
	if n == 1 {
		if grid[0][0] == 1 {
			return &oneNode
		}
		return &zeroNode
	}

	g_tmp := make([][]int, n/2)
	copy(g_tmp, grid[:n/2])
	for i := range g_tmp {
		g_tmp[i] = g_tmp[i][:n/2]
	}
	tl := construct(g_tmp)

	copy(g_tmp, grid[:n/2])
	for i := range g_tmp {
		g_tmp[i] = g_tmp[i][n/2:]
	}
	tr := construct(g_tmp)

	copy(g_tmp, grid[n/2:])
	for i := range g_tmp {
		g_tmp[i] = g_tmp[i][:n/2]
	}
	bl := construct(g_tmp)

	copy(g_tmp, grid[n/2:])
	for i := range g_tmp {
		g_tmp[i] = g_tmp[i][n/2:]
	}
	br := construct(g_tmp)

	if tl.IsLeaf && tr.IsLeaf && bl.IsLeaf && br.IsLeaf && tl.Val == tr.Val && br.Val == bl.Val && tl.Val == bl.Val {
		node_out.IsLeaf = true
		node_out.Val = tl.Val
		return &node_out
	}

	node_out.IsLeaf = false
	node_out.Val = false
	node_out.TopLeft = tl
	node_out.TopRight = tr
	node_out.BottomLeft = bl
	node_out.BottomRight = br

	return &node_out
}
