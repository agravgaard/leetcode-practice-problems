package main

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMain(m *testing.M) {
	os.Exit(m.Run())
}

func TestConstruct(t *testing.T) {
	test_grid := [][]int{{0, 1}, {1, 0}}
	test_result := construct(test_grid)

	var true_node Node
	true_node.IsLeaf = false
	true_node.Val = false
	true_node.TopLeft = &zeroNode
	true_node.TopRight = &oneNode
	true_node.BottomLeft = &oneNode
	true_node.BottomRight = &zeroNode

	assert.Equal(t, &true_node, test_result)
}

func TestConstructBigger(t *testing.T) {
	test_grid := [][]int{
		{1, 1, 1, 1, 0, 0, 0, 0},
		{1, 1, 1, 1, 0, 0, 0, 0},
		{1, 1, 1, 1, 1, 1, 1, 1},
		{1, 1, 1, 1, 1, 1, 1, 1},
		{1, 1, 1, 1, 0, 0, 0, 0},
		{1, 1, 1, 1, 0, 0, 0, 0},
		{1, 1, 1, 1, 0, 0, 0, 0},
		{1, 1, 1, 1, 0, 0, 0, 0}}

	test_result := construct(test_grid)

	var true_node Node
	true_node.IsLeaf = false
	true_node.Val = false
	true_node.TopLeft = &oneNode

	var tr Node
	tr.IsLeaf = false
	tr.Val = false
	tr.TopLeft = &zeroNode
	tr.TopRight = &zeroNode
	tr.BottomLeft = &oneNode
	tr.BottomRight = &oneNode

	true_node.TopRight = &tr
	true_node.BottomLeft = &oneNode
	true_node.BottomRight = &zeroNode

	assert.Equal(t, &true_node, test_result)
}
