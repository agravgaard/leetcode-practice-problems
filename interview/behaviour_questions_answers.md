# Behaviour questions

## Basic ideas

Insert core Value (read the job description and summarize soft skills):

* Communication
  - I've lived in a dorm for more than eight years in a hallway with 14 other people at a time and, with poeple moving in and out somewhat often, more than 100 people in total by my estimation.
  - Worked together with a new team effectively for 6 months in Boston
  - Worked with two teams of 3 and 2 enginneers supervising their bachelor projects
  - Teaching: Swimming and university
* Time management
  - I've previously employed SCRUM-like methods for managing my tasks and prioritising
  - PhD dissertation were handed in on time mainly thanks to good time management in the last two months.
* Teamwork
  - See communication + Kia & Trine
* Attention to detail
  - Sometimes a bit to much which may seem pedantic
  - Necessary as small changes sometimes even single letters can create hard to catch bugs:
    - Expressive code to have these confusions happen less often
  - Example, order of arguments in std::vector and string constructors.
* Analytical thinking
  - I think this is comes very naturally to me with my physics background
  - WEPL calculation example
* Prioritizing
  - See Time management
* Problem-solving
  - See Analytical thinking + normalizing projections example

## For example, from UberEats:

- "Comfortable working with ambiguity in constantly evolving environment"
  * I thrive in environments of constant change. ”Adaptation” was a key element in my PhD.
  * This is actually one of the most compelling sentences in the job posting, imo.
- "curiosity, passion, and collaborative spirit"
  * That's why I initially decided to mo a PhD, unfortunately the actual PhD work were different from what I hoped.
- "customer-oriented"
  * Well of course that's the motivation for most projects:
    - My PhD were motivated to have the safest possible delivery of cancer treatment without the patient having to wait in the treatment position for too long.
- "creating an app to fuel our three-sided marketplace of eaters, delivery-partners, and restaurants."
  * This project sounds really interesting, I had a similar problem in my PhD project:
    - eater = patients
    - delivery-partners = medical physicists
    - restaurants = medical doctors
    - The application should take patient data and with the help of a medical phycisist accurately calculate the personalised treatment of the day and return a visualisation and metrics a medical doctor can interpret and understand easily.

## Firstly, make BQ interview two-way conversation.
## Secondly, Feed interviewee the key points that he wants and will take notes.

Understand what this question want, give them what they want.(leadership? ownership? Competency? deep dive? deliver results? learn and be curious?)

* **Prepare 2-3 stories for every keyword.**
* **All stories must be well-structured and provide examples using metrics or data**
* **Use these stories and keywords to answer as many as possible questions.**

Practice until you feel super natural about it.

## Follow STAR principle to answer every question.[could be scenario in phd]

Situation: when, where, how the thing happened? Why it is important?
Task: how you specific your task? Describe the task.
Action: analyze the task (better to be a successful team project) and difficulties. What action/method you took? (Make yourself more actively and obviously in the story) and whom you collaborated with?
Result: what’s the result (how to define the result? Any accomplishment, for example, less cost, higher efficacy, better quality, better customer service)? What you learned from it?


## YouTube Channels

Choose your favorite one and learn his/her speaking tone and pace

- Linda Raynier:
  * [How to be Confident in Interviews](https://www.youtube.com/watch?v=jQfVn2zakKQ&list=WL&index=5&t=86s)
  * [How to Make Interviewers See You as the Right “Fit” for the Job - 5 Tips](https://www.youtube.com/watch?v=taHSZEhTzPc&list=WL&index=7)
  * [Where Do You See Yourself in 5 Years? - Ideal Sample Answer](https://www.youtube.com/watch?v=tt4TF1wqz9U&t=172s)
- Work It Daily:
  * [8 Smart Questions To Ask Hiring Managers In A Job Interview](https://www.youtube.com/watch?v=Y95eI-ek_E8&list=WL&index=11&t=1s) (4C principle：connect，culture，challenges，close. 2 sample questions for each principle.)
- Self Made Millennial


