# Keywords and questions [especially for ubereats]

"Comfortable working with ambiguity in a constantly evolving environment" requires Ownership, Flexibility, Time Management when facing to deadline and stress, and excellent Conflict Management.


## Example

Have you experienced a case when you were working on some task given by your team, and got a request from another team, what would you do?

- "Ownership" these two tasks are equally important for me.
- "Customer Obsession" Customer’s need decides the priority.
- "communication" I will have a meeting with managers and consult them.
- "dive deep" I get these two tasks because I know the best of them. So I will do my research and calculate the workload/timeline. I will present the data in the meeting.


## "Ownership", "passion", "curiosity"

Prepare 2 or 3 stories to cover all the questions

* Tell me about a time when you had to work on a project with unclear responsibilities: 
  - Well my entire PhD, I've had very little guidance and "hand-holding", I've mostly worked independently based on unclear broad strokes from my supervisor and department leaders.
* Tell me a time when you have to work with incomplete information: (are right, a lot)
  - Medical data is ripe with both missing and redundant information, the ability to infer, calculate or (gu-)estimate values has been important. E.g. beam-line for gPMC
* Will you do more than what was required: (Ownership, High Standard, Think Big)
  - Yes, I care about writing good and reliable software. In particular code expressiveness is important to me.
  - E.g. Adding tests, CI, CMake suberbuild, installer, etc. to CBCTRecon
* Tell me a time when you took on something significant outside your area of responsibility. (ownership, highest standard)
  - I've done this a lot at the dorm with my volunteering work
  - During my phd I've assisted and helped my collegues a lot:
    - They aren't as strong programmers and come to me for advise...
    - I explain how I would do it and discuss the solution with them
    - Quite often after understanding the problem, I'll offer to write the code for them or improve their code to do what they want
    - E.g. the WEPL project ended up being joint-author.
    - E.g. Jesper's python scripts, memory management, threading, etc. (no authorship from most of these)
* Describe a time when you saw some problem and initiative to correct it instead of waiting for someone else to do it: (bias for action, ownership)
  - GitLab-runner `--gpu` support for docker
  - ROCm AUR. DKMS didn't compile on newer linux, fix errors and send merge-request with patches for AUR and AMD repo.
  - RTK Xim-reader
  - Plastimatch ITKv5 support
* Tell me a time that you need to get information from someone who is not very responsible? What did you do? (bias for action, ownership, hire and develop the best)
  - E.g. gPMC random bug
  - CB patient data
* Tell me about a time when you went above and beyond the call of duty in order to get a job done.
  - CBCTRecon, most of the work that went into this application were never published and only described in a few sentences in the dissertation.
* Give me an example of when you showed initiative and took the lead.
  - Supervising the 5 ingeneers

## "Technical Expertise", "passion", "curiosity", "customer-oriented"

Prepare 2 or 3 stories to cover all the questions

* what did you learn from the recent project?
  - Any technical difficulties?
    * Many, mostly compatibility, e.g. compilers library and API versions
    * GPU compatibility CUDA is Nvidia only, OpenCL is an open standard, but the nvidia drivers are buggy.
    * Making GPUs available in docker images for CI
  - How will you improve/breakthrough it? (Dive Deep, Ownership, Think Big)
    * CMake checks, good documentation and automate the process, update when compatibility changes
    * Rewrite CUDA code in OpenCL, but use CUDA when Nvidia gpu is available for performance and reliability.
    * Merge-request to gitlab-runner, add runners on personal computers with different flags to pass through GPUs.
* Tell me about a time you were effective in putting your technical expertise to use to solve a problem.
  * Data formatting, regex with python, vim, sed, grep has been extremely useful and my collegues has come to me to get this job done.
* Describe a time where you were required to use a certain technology that you did not know well.
  * I did not know C++ before I started the CBCTRecon project, but found myself inhereting a large C++ project. Within a few months I was able to compile the code, make changes and add new functionality.
* Describe and explain a technical concept to me as if I'm a new grad person joining your team.
  - Very technical:
    * ABI breakage
  - Medium technical:
    * floating point precision
  - Slightly technical:
    * test-driven development
* Tell me your most challenging project/ a time you solved a complex problem
  - explain how complex it is, tight deadline or complicated technology?
    * gPMC wrapper and invisible geometry
    * Xim reader - proprietary binary fileformat with limited docs.
  - how you solved it and what you learned?
    * Wrote a CLI and a geometry visualizer in Qt
    * Re-wrote a Matlab script with poor perf and compared against similar file-type reader.


## "Flexibility" - the point is "Bias for Action"

Could finish the project quickly and deliver the result in time; do more for now rather than insist perfectness, correct it afterwards; Exploring possible solutions for ambiguity questions; could make quick decisions for different solutions.

Prepare 2 or 3 stories to cover all the questions

* With examples, convince me that you can adapt to a wide variety of people, situations and environments.
  * Collab with Team in Boston, Engineers, Trine, Kia, Jesper
  * Dorm-life
* Describe a time when the scope or parameters of what you were working on changed mid-way through.
  * This happened often early on, e.g. during bachelor project, CBCT $`\to`$ rCT and when I learned that registration was a thing.
  * Changing dose-optimizer to Eclipse.
  * Changing data source.
* Tell me a time when you came up with a simple solution to solve a problem (simple solution is not the better solution. It allows the tradeoff of lower accuracy but has to be stable and reliable.)
  * Projection normalization.
  * unecessesary float $`\to`$ UShort $`\to`$ float conversions removed and re-implemented.
  * CLI wrapper in VS2013 to keep using latest compiler in main app.
* Tell me a time when you are at 75% completion of your task but have to pivot strategy, how did you make it success? (deliver results, bias for action, earn trust)
  * See changing midway above
  * Several changes to WEPL metric and registration methods

## "deadline" "Stress" "Time Management" — how to Prioritize

Prepare 2 or 3 stories to cover all the questions

* Give an example of a time in which you had to be relatively quick in coming to a decision.
  - Practically every single abstract deadline
* Describe a time when you were faced with a stressful situation that demonstrated your coping skills.
  - I've always been able to keep focus and calm right up until the last seconds of a deadline, my supervisor less so and says "I must have ice in my stomach" (possibly a norwegian expression)
* Tell me about a situation in which you had to deal with a very upset client.
  - During a conference we were in contact with a system vendor for collaboration
  - My supervisor asked me to send them my abstract and a brief summary of my project
  - I wrote it up quickly and asked for his approval, he said OK and that I should add the department head to CC
  - The department leader responded promptly that a lowly PhD student should not interfere with ongoing negotiations between vendor and department
  - My supervisor was very upset, I didn't care much (I've heard rumors that he behaves that way)
  - We wrote a polite mail saying pulling a bit back claiming that the work was actually very preliminary (it really wasn't but we had to reduce importance of the mail) and we would wait with further discussion until after the "ongoing negotiations".
  - I never heard the results of those negotiations, the department leader hold his cards tightly.
* Tell me about a time when you had many things to do and you were required to prioritize your tasks.
  - Dissertation is the latest one, include H&N project proposal
* Describe how you would handle the situation when multiple people are waiting for your help.
  - Scrum, quick and valuable first, difficult and low value last + inform those who will be last in the queue.
* When you have had to handle a variety of assignments, what are the results?  (bias for action, deliver results)
  - Mostly good, when the assignments are organised and written down (e.g. gitlab issues or checklists)
  - Bad when I attempt to do it by memory
* Tell me a time when you missed the deadline, explain the situation:  (deliver result, ownership, earn trust, highest standard, a bias for action)
  - Why missed?
    * Abstract dealines, faulty results or not enough compute time
  - How you minimize the loss?
    * Fix the method or have it computed before next conference abstract deadline. (sometimes the deadlines were extended - no loss)
  - The final result is good?
    * Mostly, sometimes removing the faultyness of the results didn't make the conclusion stronger.
* Tell me about a time when you have to work with limited time or resources. (fragility, ownership, a bias for action, deliver result)
  * Well most of my PhD I've been a one-man army, I've had the opportunity to collaborate some times, but I am for example the only one around who knows C++
  * Limited compute ressources lead me to improving my algorithms and threading in application and even to GPU programming.
  * Limited time I think has been covered sufficiently above by now.

## "deadline and Stress could cause mistakes" - the point is how you solve it and the result is good.

Prepare 2 stories to cover all the questions

* Tell me a time when you received negative feedback/when you were wrong:
  - This is quite rare. I've been wrong quite a few times, but it's been a learning experience with more or less quick corrections, wouldn't call it negative feedback, but constructive.
* The biggest mistake you made and what did you learn from it?
  - Maybe that I broke the application viewer window almost completely once and it took me months to find the error - turned out to be a parenthesis around a static_cast with division. I learned to be more careful with my refactoring, to test properly and between changes.
* Tell me a time when you take a calculated risk, failed/succeed (bias for action, ownership)
  - H&N Project proposal


## "Conflict Management" (this topic could answer so many categories of questions)

Prepare 2 or 3 stories to cover all the questions

* Tell me about a time when you had a conflict with someone in authority and how you handled it.
  - See upset client above
* Give an example of when you had to work with someone who was difficult to get along with.
  - Luckily this has been quite rare
  - Residents union, arrogant type with many bad ideas and opinions on tiny matters.
  - Be patient, try to listen, gather their point of view, and explain clearly.
* How to handle different opinions in the workplace
  - example: tabs vs. spaces, (doesn't actually matter).
  - Create a style guide and use auto-formatting
  - Are there any tools that work better with one, pros/cons
  - Maybe a vote
  - Typically it's just talk and the first to take action gets to decide, e.g. github vs. gitlab
* Tell me about a time that your teammate did not meet your expectation.
  - Engineers created some half-baked solutions and didn't understand the physics
  - Probably my mistake for having to high expectations and not explaining properly, but I only got positive feedback during the development process.
* Give me a specific example of a time when you had to conform to a policy with which you did not agree.
  - To be allowed to use the research TPS of the hospital I had to agree to a policy on data "security", but there was nothing secure about it.
* Give me an example of a time you discovered an error that had been overlooked by a colleague, what did you do? (Attention to Detail)
  - Kia registration errors
  - Jesper memory leaks
  - Engineers mutex

These seem more like statements/advise than questions to me

* Explain conflict. Describe it as a conflict of responsibility rather than personality.
* Show empathy but dare to say NO and declare your own opinions.
* Strong will to communication (through "communication", you figure out the reason of conflict and do tradeoff or de-escalate it, for the sake of the whole team. "collaborative spirit" "Leadership"" Teamwork").
* Summarize the change before and after your efforts to deal with conflicts.

## "Communication"

* Tell me about a time when you had to use your presentation skills to influence someone's opinion.
  - During meetings with the residents union in particular
  - Sometimes during journal presentations (people over-criticising or praising a paper)
* Describe a situation in which you were able to use persuasion to successfully convince someone to see things your way.
  - The specifics are hard to remember, but mostly it was about a guy arguing over non-problem issues and forgetting the main purpose of the residents union reminding him in a clear way, while listening to him persuaded the board more quickly than I expected.

# "Leadership", "Teamwork"

* Tell me about a situation when you lead a group of people to achieve a goal.
* Tell me about a time when you delegated a project effectively.
* Tell me about an occasion when you worked with a group to complete a project.
  - The engineers project, the UI project and the RT data handler.
* Describe how you were able to successfully contribute to a multi-interdisciplinary team.
  - Well that's my whole PhD is it not?
* Tell me about a time you mentored someone.
  - The bachelor project of Trine is probably the best example.
