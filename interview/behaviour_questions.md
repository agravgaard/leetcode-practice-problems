# Behaviour questions

## Basic ideas

Insert core Value (read the job description and summarize soft skills):
* communication
* time management
* teamwork
* attention to detail
* analytical thinking
* prioritizing
* problem-solving

For example from Ubereat,
- "Comfortable working with ambiguity in constantly evolving environment"
- "curiosity, passion, and collaborative spirit"
- "customer-oriented (Replace customer with your supervisor and colleagues in PhD)"
- "creating an app to fuel our three-sided marketplace of eaters, delivery-partners, and restaurants."

## Firstly, make BQ interview two-way conversation.
## Secondly, Feed interviewee the key points that he wants and will take notes.

Understand what this question want, give them what they want.【leadership? ownership? Competency? deep dive? deliver results? learn and be curious?】

* **Prepare 2-3 stories for every keyword.**
* **All stories must be well-structured and provide examples using metrics or data**
* **Use these stories and keywords to answer as many as possible questions.**
Practice until you feel super natural about it.

## Follow STAR principle to answer every question.[could be scenario in phd]

Situation: when, where, how the thing happened? Why it is important?
Task: how you specific your task? Describe the task.
Action: analyze the task (better to be a successful team project) and difficulties. What action/method you took? (Make yourself more actively and obviously in the story) and whom you collaborated with?
Result: what’s the result (how to define the result? Any accomplishment, for example, less cost, higher efficacy, better quality, better customer service)? What you learned from it?


## YouTube Channels

Choose your favorite one and learn his/her speaking tone and pace

- Linda Raynier:
  * [How to be Confident in Interviews](https://www.youtube.com/watch?v=jQfVn2zakKQ&list=WL&index=5&t=86s)
  * [How to Make Interviewers See You as the Right “Fit” for the Job - 5 Tips](https://www.youtube.com/watch?v=taHSZEhTzPc&list=WL&index=7)
  * [Where Do You See Yourself in 5 Years? - Ideal Sample Answer](https://www.youtube.com/watch?v=tt4TF1wqz9U&t=172s)
- Work It Daily:
  * [8 Smart Questions To Ask Hiring Managers In A Job Interview](https://www.youtube.com/watch?v=Y95eI-ek_E8&list=WL&index=11&t=1s) (4C principle：connect，culture，challenges，close. 2 sample questions for each principle.)
- Self Made Millennial


