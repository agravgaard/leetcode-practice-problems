# System design

## System design problems that have been asked during Ubereats interviews:

* Figure out the system of Uber eats [by searching blog, articles and papers]
* Design Uber eats restaurant recommendation list [not related to recommendation ML algorithm]
* design query search of restaurant by geo (location search module) and category (preset category filter or by group)
* Design the feature of “Zoom in” and “checking the heat map at specific time”(geo hash+ ring pop+ casendra/DynamoDB; server, database, sharding; calculate qps and storage)
* Personalization, scalability strategy, such as offline ML component，
* async，how to prevent server overload
* Caching
* Consistent Hashing…
* Design a menu storage system for uber eats
* Design a metrics system


Design a youtube video view count system
Design Facebook messager
Design wechat 
Design a distributed unique id generator
Design a monitoring system
Design lightning deal


## Analyzing process 
1. Ability to break down hard problems
2. Fundamental knowledge about distributed systems
3. Ability to think through trade-offs

## [time management in 40mins]
1. narrow down functional/non-functional requirements （5min)
2. traffic estimation (2min)
3. draw flow chart (rest）

## Learning resource:
* systemexpert [basically enough for new grad’s interview] 
* [for building knowledge system](https://github.com/donnemartin/system-design-primer)
* [for specific problems](https://www.youtube.com/channel/UCn1XnDWhsLS5URXTi5wtFTA) 


