# Behavioural question process

## Intro.

* Clear and brief. Less than 1 min.

## Basic questions.

* Why Uber EATS? what interests you in this position?[do your research]
* The best value you wish the company could have
* Favorite programming language and why

## Dive deep in project

* Detailedly about your cv project and PhD. Answer in STAR principles.
* Prepare for those keywords in job description.


# Questions has been asked in previous Uber Eats interviews

**[Must Prepare]**

* Feature quality vs feature deadline (with delaying or without the feature)
* Will Uber be beaten by (competitor companies, for example, Just Eat in Aarhus)?


## Do you have any questions for interviewer?

- "How would you describe your company/team’s culture?"
- "How do the team leaders help the team grow professionally?"
- "Why you choose to join this company, what was your career plan before got into this role?"
- "What do you think is your biggest takeaway from this position"
- "What do you suggest I do to prepare for or succeed in this position."
- "What have the past employees do to succeed and could you give me some pointers as well?”

