# Keywords and questions [especially for ubereats]

"Comfortable working with ambiguity in a constantly evolving environment" requires Ownership, Flexibility, Time Management when facing to deadline and stress, and excellent Conflict Management.


## Example

Have you experienced a case when you were working on some task given by your team, and got a request from another team, what would you do?

- "Ownership" these two tasks are equally important for me.
- "Customer Obsession" Customer’s need decides the priority.
- "communication" I will have a meeting with managers and consult them.
- "dive deep" I get these two tasks because I know the best of them. So I will do my research and calculate the workload/timeline. I will present the data in the meeting.


## "Ownership", "passion", "curiosity"

Prepare 2 or 3 stories to cover all the questions

* Tell me about a time when you had to work on a project with unclear responsibilities: 
* Tell me a time when you have to work with incomplete information: (are right, a lot)
* Will you do more than what was required: (Ownership, High Standard, Think Big)
* Tell me a time when you took on something significant outside your area of responsibility. (ownership, highest standard)
* Describe a time when you saw some problem and initiative to correct it instead of waiting for someone else to do it: (bias for action, ownership)
* Tell me a time that you need to get information from someone who is not very responsible? What did you do? (bias for action, ownership, hire and develop the best)
* Tell me about a time when you went above and beyond the call of duty in order to get a job done.
* Give me an example of when you showed initiative and took the lead.

## "Technical Expertise", "passion", "curiosity", "customer-oriented"

Prepare 2 or 3 stories to cover all the questions

* what did you learn from the recent project?
  - Any technical difficulties?
  - How will you improve/breakthrough it? (Dive Deep, Ownership, Think Big)  
* Tell me about a time you were effective in putting your technical expertise to use to solve a problem.
* Describe a time where you were required to use a certain technology that you did not know well.
* Describe and explain a technical concept to me as if I'm a new grad person joining your team.
* Tell me your most challenging project/ a time you solved a complex problem
  - explain how complex it is, tight deadline or complicated technology?
  - how you solved it and what you learned?


## "Flexibility" - the point is "Bias for Action"

Could finish the project quickly and deliver the result in time; do more for now rather than insist perfectness, correct it afterwards; Exploring possible solutions for ambiguity questions; could make quick decisions for different solutions.

Prepare 2 or 3 stories to cover all the questions

* With examples, convince me that you can adapt to a wide variety of people, situations and environments. 
* Describe a time when the scope or parameters of what you were working on changed mid-way through.
* Tell me a time when you came up with a simple solution to solve a problem (simple solution is not the better solution. It allows the tradeoff of lower accuracy but has to be stable and reliable.)
* Tell me a time when you are at 75% completion of your task but have to pivot strategy, how did you make it success? (deliver results, bias for action, earn trust)

## "deadline" "Stress" "Time Management" — how to Prioritize

Prepare 2 or 3 stories to cover all the questions

* Give an example of a time in which you had to be relatively quick in coming to a decision.
* Describe a time when you were faced with a stressful situation that demonstrated your coping skills.
* Tell me about a situation in which you had to deal with a very upset client.
* Tell me about a time when you had many things to do and you were required to prioritize your tasks.
* Describe how you would handle the situation when multiple people are waiting for your help.
* When you have had to handle a variety of assignments, what are the results?  (bias for action, deliver results)
* Tell me a time when you missed the deadline"explain the situation. Why missed? How you minimize the loss? The final result is good" (deliver result, ownership, earn trust, highest standard, a bias for action)
* Tell me about a time when you have to work with limited time or resources. (fragility, ownership, a bias for action, deliver result)

## "deadline and Stress could cause mistakes" - the point is how you solve it and the result is good.

Prepare 2 stories to cover all the questions

* Tell me a time when you received negative feedback/when you were wrong:
* The biggest mistake you made and what did you learn from it?
* Tell me a time when you take a calculated risk, failed/succeed (bias for action, ownership)


## "Conflict Management" (this topic could answer so many categories of questions)

Prepare 2 or 3 stories to cover all the questions

* Tell me about a time when you had a conflict with someone in authority and how you handled it.
* Give an example of when you had to work with someone who was difficult to get along with.
* how to handle different opinions in the workplace，
* Tell me about a time that your teammate did not meet your expectation.
* Give me a specific example of a time when you had to conform to a policy with which you did not agree.
* Give me an example of a time you discovered an error that had been overlooked by a colleague, what did you do? (Attention to Detail)

* Explain conflict. Describe it as a conflict of responsibility rather than personality.
* Show empathy but dare to say NO and declare your own opinions.
* Strong will to communication (through "communication", you figure out the reason of conflict and do tradeoff or de-escalate it, for the sake of the whole team. "collaborative spirit" "Leadership"" Teamwork").
* Summarize the change before and after your efforts to deal with conflicts.

## "Communication"

* Tell me about a time when you had to use your presentation skills to influence someone's opinion.
* Describe a situation in which you were able to use persuasion to successfully convince someone to see things your way.

# "Leadership", "Teamwork"

* Tell me about a situation when you lead a group of people to achieve a goal.
* Tell me about a time when you delegated a project effectively.
* Tell me about an occasion when you worked with a group to complete a project.
* Describe how you were able to successfully contribute to a multi-interdisciplinary team.
* Tell me about a time you mentored someone.

