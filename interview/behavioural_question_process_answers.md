# Behavioural question process

## Intro.

* Clear and brief. Less than 1 min.

## Basic questions.

* Why Uber EATS? what interests you in this position?[do your research]
  - The focus on the 3-sided user-base makes UberEats stand out, which I think makes it more promising than competitors, imo.
  - Reading the job-posting gave me the impression that you have a team-spirit with passion for the project.
  - I want to help you make your platform the best it can be.
* The best value you wish the company could have
  - Maybe passion for the project:
    - During my PhD one of my biggest dissapointments were how little the professors and department leaders actually cared for improving patient care/treatment, almost encouraging obfuscation to improve their own position for applying funding.
    - I'm not saying that profit can't be a motivator, just that it shouldn't be the only one.
* Favorite programming language and why
  - C++ (Rust also has most of these, but its still a bit too young)
    - strong types
    - function templates helps DRY
    - Memory management by scope (rather than manual as in C)
    - compiled language:
      - high perf.
      - (Almost) Zero cost to be expressive
      - static analysis

## Dive deep in project

* Detail about your CV projects and PhD. Answer in STAR principles.
  - STAR = (Situation), Task, Action, Results, i.e. basically the abstract format
* Prepare for those keywords in job description.
  - I don't think I put anything to foreign in there

# Questions has been asked in previous Uber Eats interviews

**[Must Prepare]**

* Feature quality vs feature deadline (with delaying or without the feature)
  - Good question, you'll want to avoid a Cyberpunk 2077 situation
  - During preparation of abstracts and manuscripts I've often found flaws or weak results soon before deadlines and either attempted to improve, replace or remove these, or accepts the flaws as is.
  - A common problem were image registration errors (e.g. inverted y-axis) were the images were the registration shift were small most of the time and unnoticable, but could create outliers.
    - Remove outliers from results? Not a great option, but if there's no time it'll have to do for now and we'll correct it later, e.g. before conference.
    - Attempt to correct the mistake? Ideal solution, but do we have the time? Maybe we can redo just the outliers? How much re-calculation and re-analysis is necessary?
    - Submit with flaws? If time is short, report it as a known defect and estimate error impact on results.
* Will Uber be beaten by (competitor companies, for example, Just Eat in Aarhus)?
  - I hope UberEats will be available in DK eventually, I think the concept is to offer a more 3-sided platform
  - Most competitors are focused on only delivery as a company and users as customers, like WOLT and just-eat, I think the UberEats is more open and maybe inclusive in that way


## Do you have any questions for interviewer?

- "How would you describe your company/team’s culture?"
- "How do the team leaders help the team grow professionally?"
- "Why you choose to join this company, what was your career plan before got into this role?"
- "What do you think is your biggest takeaway from this position"
- "What do you suggest I do to prepare for or succeed in this position."
- "What have the past employees do to succeed and could you give me some pointers as well?”

