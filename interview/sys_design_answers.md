# System design

## System design problems that have been asked during Ubereats interviews:

* Figure out the system of Uber eats [by searching blog, articles and papers]
* Design Uber eats restaurant recommendation list [not related to recommendation ML algorithm]
  - Location, popularity, taste-profile (italian, chinese, etc., or inferred by ML), weighted sort.
* design query search of restaurant by geo (location search module) and category (preset category filter or by group)
  - Restaurants in database at location and rounded-to-grid-location, as well as category to DB.
  - Get current location, or ask user for input.
  - round current location to grid and `SELECT * WHERE rounded_cur_loc == rounded_loc + (+/-1, +/-1)` (psuedo code)
  - then sort output on actual closest location.
* Design the feature of “Zoom in” and “checking the heat map at specific time”(geo hash+ ring pop+ casendra/DynamoDB; server, database, sharding; calculate qps and storage)
  - Zoom should discard out-of-frame location from sort and draw the new top-N.
  - If heat map means "busy delivery zone":
    - Get locations of all drivers in area-based databases
    - On server calculate density function in area at every timestep.
    - provide area-based DF to users by map-zoom (only request on sufficient zoom-level)
* Personalization, scalability strategy, such as offline ML component，
  - ML taste profile based on provided info, previous searches and deliveries.
  - Upload data for updating model at regular intervals
  - Train model on new (or new+old *if model allows*) data
  - inference with user data input at reasonable rate depending on new data available from user and new models available from server.
* async，how to prevent server overload
  - Kubernetes mostly solves this problem.
  - Provided functions as micro-services.
  - Queue requests and accept as ressources are available.
  - Additionally, request time-out or cancelling could be an option.
  - Additionally, queue priority, i.e. only train models when traffic is low.
* Caching
  - Cookies?
  - Store expensive to transfer data on user-device and ideally only update when necessary.
* Consistent Hashing…
  - Depends on whats needed, in general use a good FOSS hashing algorithm, MD(N), SHA-(N), BLAKE(N.), especially if security is important.
* Design a menu storage system for uber eats
  - Make a class that represent the possible structure, and a wrapper to JSON (or similar)
  - ```cpp
    #include <...>

    class Currency {
      public:
        Currency get_value(const std::string &in_currency_type) {...}
      private:
        double value;
        std::string currency_type;
    }

    class MenuItem {
      std::string name;
      Currency price;
      std::vector<std::string> ingredients;
    }

    class Menu {
      public:
        explicit Menu(const std::string &name) : _menu_name(name) {}
        void add_item(...); // maybe ingredients are optional?
        bool save_as_json(const std::filesystem::path &filename);
      private:
        std::string _menu_name;
        std::vector<MenuItem> _items;
    }
    ```
* Design a metrics system
  - Depends on what you mean by metrics.
  - user activity metrics?
    - app openings/day
    - in-app time
    - searches, taste-profile, etc.
    - deliveries /day, /week, /month
    - /category, /kitchen, /area
  - server load metrics?
    - Kubernetes has some really nice tools for this.


Design a youtube video view count system
 - YouTube uses watch-time as an important metric.
 - Require a certain amount of watch-time, e.g. 15 seconds, for counting a view.

Design Facebook messenger
Design wechat (IDK the difference from messenger)
 - Messages must be stored encrypted.
 - Use auth token to allow decryption and perform on device
 - Save latest messages on devices (in cookie/cache)
 - The chat interface is somewhat easy to implement (it's a common example for learning web-languages)
 - The security and available everywhere is the more difficult part.

Design a distributed unique id generator
 - Psuedo-RNG seeded by time to hash, query server if ID already exists, if not store as new user ID
 - If it does already exists attempt to repeat.

Design a monitoring system
 - Monitoring what?
 - See metrics system above

Design lightning deal
 - Not sure what this is, but I assume a time-limited discount
 - Provide LD interface in app + a way to show
 - query server from device if any LD is available
   - If multiple request the highest recommended
 - download neccesary content to tmp storage
 - show in app


## Analyzing process 

1. Ability to break down hard problems
  - Structure based reverse WEPL method:
    - WEPL is line-integral in space
    - line is $`\vec{p} = t \cdot \vec{n} + \vec{p_0}`$
    - step from inside $`t=0`$ and out $`t \to \infty`$
    - neat geometry tricks for making stepping finite as fast as possible:
      - i.e. intersection with bounding box
    - re-use components from WEPL calculations for reverse calculation
2. Fundamental knowledge about distributed systems
  - GitLab-runner, HTTP, SSH, OpenCL
3. Ability to think through trade-offs.
  - My personal list of preference goes readability $`\to`$ correctness $`\to`$ performance.
  - I Like the KISS and DRY principles.
  - I know how quickly C++ code can become complicated and hard to maintain, so I strive to create expressive/understandable code and interfaces


## Learning resource:
* systemexpert [basically enough for new grad’s interview] 
* [for building knowledge system](https://github.com/donnemartin/system-design-primer)
* [for specific problems](https://www.youtube.com/channel/UCn1XnDWhsLS5URXTi5wtFTA) 


